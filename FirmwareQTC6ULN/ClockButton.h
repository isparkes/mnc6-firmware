#ifndef ClockButton_h
#define ClockButton_h

#include "Arduino.h"

class ClockButton
{
  public:
    ClockButton(int inputPin);
    void checkButton(unsigned long nowMillis);
    void reset();
    boolean isButtonPressedNow();
    boolean isButtonPressed();
    boolean isButtonPressed1S();
    boolean isButtonPressed2S();
    boolean isButtonPressed8S();
    boolean isButtonPressedAndReleased();
    boolean isButtonPressedReleased1S();
    boolean isButtonPressedReleased2S();
    boolean isButtonPressedReleased8S();
    unsigned long buttonLastPressedMillis();
    void inhibit(unsigned long nowMillis, int millisToInhibit);
  private:
    int _inputPin;
    int  _buttonPressedCount = 0;
    unsigned long _buttonPressStartMillis = 0;
    unsigned long _buttonLastPressedMillis = 0;
    const byte _debounceCounter = 5; // Number of successive reads before we say the switch is down
    boolean _buttonWasReleased = false;
    boolean _buttonPress8S = false;
    boolean _buttonPress2S = false;
    boolean _buttonPress1S = false;
    boolean _buttonPress = false;
    boolean _buttonPressRelease8S = false;
    boolean _buttonPressRelease2S = false;
    boolean _buttonPressRelease1S = false;
    boolean _buttonPressRelease = false;
    unsigned long _inhibitEndTime = 0;
    
    void checkButtonInternal(unsigned long nowMillis);
    void resetInternal();
};

#endif
