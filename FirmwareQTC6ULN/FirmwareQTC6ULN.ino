//**********************************************************************************
//* Code for a ULN2003 based Nixie clock.                                          *
//*                                                                                *
//*  nixie@protonmail.ch                                                           *
//*                                                                                *
//**********************************************************************************
//**********************************************************************************

// Standard Libraries
#include <avr/io.h>
#include <EEPROM.h>
#include <Wire.h>
#include <avr/wdt.h>

// Clock specific libraries, install these with "Sketch -> Include Library -> Add .ZIP library
// using the ZIP files in the "libraries" directory
#include <DS3231.h>             // https://github.com/NorthernWidget/DS3231 (Wickert 1.0.2)
#include <TimeLib.h>            // https://playground.arduino.cc/Code/Time/ (Margolis 1.5.0)
#include <NeoPixelBus.h>        // https://github.com/Makuna/NeoPixelBus (Makuna 2.6.2)

// Other parts of the code, broken out for clarity
#include "ClockButton.h"
#include "Transition.h"
#include "DisplayDefs.h"
#include "I2CDefs.h"

// Software version shown in config menu
#define SOFTWARE_VERSION      102
#define NORMAL_DISPLAY           // FLIP_DISPLAY | NORMAL_DISPLAY


//**********************************************************************************
//**********************************************************************************
//*                               Constants                                        *
//**********************************************************************************
//**********************************************************************************
#define EE_12_24              1      // 12 or 24 hour mode
#define EE_FADE_STEPS         2      // How quickly we fade, higher = slower
#define EE_DATE_FORMAT        3      // Date format to display
#define EE_DAY_BLANKING       4      // Blanking setting
#define EE_DIM_DARK_LO        5      // Dimming dark value
#define EE_DIM_DARK_HI        6      // Dimming dark value
#define EE_BLANK_LEAD_ZERO    7      // If we blank leading zero on hours
#define EE_DIGIT_COUNT_HI     8      // The number of times we go round the main loop
#define EE_DIGIT_COUNT_LO     9      // The number of times we go round the main loop
#define EE_SCROLLBACK         10     // if we use scollback or not
#define EE_FADE               11     // if we use fade or not
#define EE_PULSE_LO           12     // The pulse on width for the PWM mode
#define EE_PULSE_HI           13     // The pulse on width for the PWM mode
#define EE_SCROLL_STEPS       14     // The steps in a scrollback
#define EE_BACKLIGHT_MODE     15     // The back light node
#define EE_DIM_BRIGHT_LO      16     // Dimming bright value
#define EE_DIM_BRIGHT_HI      17     // Dimming bright value
#define EE_DIM_SMOOTH_SPEED   18     // Dimming adaptation speed
#define EE_RED_INTENSITY      19     // Red channel backlight max intensity
#define EE_GRN_INTENSITY      20     // Green channel backlight max intensity
#define EE_BLU_INTENSITY      21     // Blue channel backlight max intensity
#define EE_SUPPRESS_ACP       23     // Do we want to suppress ACP during dimmed time
#define EE_HOUR_BLANK_START   24     // Start of daily blanking period
#define EE_HOUR_BLANK_END     25     // End of daily blanking period
#define EE_CYCLE_SPEED        26     // How fast the color cycling does it's stuff
#define EE_MIN_DIM_LO         30     // The min dim value
#define EE_MIN_DIM_HI         31     // The min dim value
#define EE_NEED_SETUP         33     // used for detecting auto config for startup. By default the flashed, empty EEPROM shows us we need to do a setup 
#define EE_USE_LDR            34     // if we use the LDR or not (if we don't use the LDR, it has 100% brightness
#define EE_BLANK_MODE         35     // blank tubes, or LEDs or both
#define EE_SLOTS_MODE         36     // Show date every now and again
#define EE_PIR_TIMEOUT_LO     37     // The PIR timeout time in S, low byte
#define EE_PIR_TIMEOUT_HI     38     // The PIR timeout time in S, high byte
#define EE_PIR_PULLUP         39     // If we use the internal pullup on the PIR pin (false = use 3V3 PIR, PIR connected, true = no PIR connected or 5V PIR)
#define EE_MIN_DIM_LO         40     // The minimum dimming value, low byte
#define EE_MIN_DIM_HI         41     // The minimum dimming value, high byte

#define COUNTS_PER_DIGIT       20
#define COUNTS_PER_DIGIT_DIM   8

// how often we make reference to the external time provider
#define READ_TIME_PROVIDER_MILLIS 60000 // Update the internal time provider from the external source once every minute

#define MIN_DIM_DEFAULT       100  // The default min dim count
#define MIN_DIM_MIN           100  // The minimum min dim count
#define MIN_DIM_MAX           500  // The maximum min dim count

#define MAX_DIM               999  // The maximum dim count

#define SENSOR_LOW_MIN        0
#define SENSOR_LOW_MAX        900
#define SENSOR_LOW_DEFAULT    100  // Dark
#define SENSOR_HIGH_MIN       0
#define SENSOR_HIGH_MAX       900
#define SENSOR_HIGH_DEFAULT   700 // Bright

#define SENSOR_SMOOTH_READINGS_MIN     1
#define SENSOR_SMOOTH_READINGS_MAX     255
#define SENSOR_SMOOTH_READINGS_DEFAULT 100  // Speed at which the brighness adapts to changes

#define BLINK_COUNT_MAX                500   // The number of impressions between blink state toggle

// How quickly the scroll works
#define SCROLL_STEPS_DEFAULT 4
#define SCROLL_STEPS_MIN     1
#define SCROLL_STEPS_MAX     40

// The number of dispay impessions we need to fade by default
// 100 is about 1 second
#define FADE_STEPS_DEFAULT 30
#define FADE_STEPS_MAX     200
#define FADE_STEPS_MIN     20

#define SECS_MAX  60
#define MINS_MAX  60
#define HOURS_MAX 24

#define COLOUR_CNL_MAX                  15
#define COLOUR_RED_CNL_DEFAULT          15
#define COLOUR_GRN_CNL_DEFAULT          0
#define COLOUR_BLU_CNL_DEFAULT          0
#define COLOUR_CNL_MIN                  0

// Clock modes - normal running is MODE_TIME, other modes accessed by a middle length ( 1S < press < 2S ) button press
#define MODE_MIN                        0
#define MODE_TIME                       0

// Time setting, need all six digits, so no flashing mode indicator
#define MODE_HOURS_SET                  1
#define MODE_MINS_SET                   2
#define MODE_SECS_SET                   3
#define MODE_DAYS_SET                   4
#define MODE_MONTHS_SET                 5
#define MODE_YEARS_SET                  6

// Basic settings
#define MODE_12_24                      7  // 0 = 24, 1 = 12
#define HOUR_MODE_DEFAULT               false
#define MODE_LEAD_BLANK                 8  // 1 = blanked
#define LEAD_BLANK_DEFAULT              false
#define MODE_SCROLLBACK                 9  // 1 = use scrollback
#define SCROLLBACK_DEFAULT              false
#define MODE_FADE                       10 // 1 = use fade
#define FADE_DEFAULT                    false
#define MODE_DATE_FORMAT                11 // 
#define MODE_DAY_BLANKING               12 // 
#define MODE_HR_BLNK_START              13 // skipped if not using hour blanking
#define MODE_HR_BLNK_END                14 // skipped if not using hour blanking
#define MODE_SUPPRESS_ACP               15 // 1 = suppress ACP when fully dimmed
#define SUPPRESS_ACP_DEFAULT            true

#define MODE_USE_LDR                    16 // 1 = use LDR, 0 = don't (and have 100% brightness)
#define MODE_USE_LDR_DEFAULT            true

#define MODE_BLANK_MODE                 17 // 
#define MODE_BLANK_MODE_DEFAULT         BLANK_MODE_BOTH

// Display tricks
#define MODE_FADE_STEPS_UP              18 // 
#define MODE_FADE_STEPS_DOWN            19 // 
#define MODE_DISPLAY_SCROLL_STEPS_UP    20 // 
#define MODE_DISPLAY_SCROLL_STEPS_DOWN  21 // 

#define MODE_SLOTS_MODE                 22 // 

// PIR
#define MODE_PIR_TIMEOUT_UP             23 // 
#define MODE_PIR_TIMEOUT_DOWN           24 // 

// Back light
#define MODE_BACKLIGHT_MODE             25 // 
#define MODE_RED_CNL                    26 // 
#define MODE_GRN_CNL                    27 // 
#define MODE_BLU_CNL                    28 // 
#define MODE_CYCLE_SPEED                29 // 

// Minimum dimming value for old tubes
#define MODE_MIN_DIM_UP                 30 // 
#define MODE_MIN_DIM_DOWN               31 // 

// Use the PIR pin pullup resistor - Not Available over I2C
#define MODE_USE_PIR_PULLUP             32 // 1 = use Pullup
#define USE_PIR_PULLUP_DEFAULT          true

// Temperature
#define MODE_TEMP                       33 // 

// Software Version
#define MODE_VERSION                    34 // 

// Tube test - all six digits, so no flashing mode indicator
#define MODE_TUBE_TEST                  35

#define MODE_MAX                        35

// Temporary display modes - accessed by a short press ( < 1S ) on the button when in MODE_TIME
#define TEMP_MODE_MIN                   0
#define TEMP_MODE_DATE                  0 // Display the date for 5 S
#define TEMP_MODE_TEMP                  1 // Display the temperature for 5 S
#define TEMP_MODE_LDR                   2 // Display the normalised LDR reading for 5S, returns a value from 100 (dark) to 999 (bright)
#define TEMP_MODE_VERSION               3 // Display the version for 5S
#define TEMP_IP_ADDR12                  4 // IP xxx.yyy.zzz.aaa: xxx.yyy
#define TEMP_IP_ADDR34                  5 // IP xxx.yyy.zzz.aaa: zzz.aaa
#define TEMP_IMPR                       6 // number of impressions per second
#define TEMP_MODE_MAX                   6

#define DATE_FORMAT_MIN                 0
#define DATE_FORMAT_YYMMDD              0
#define DATE_FORMAT_MMDDYY              1
#define DATE_FORMAT_DDMMYY              2
#define DATE_FORMAT_MAX                 2
#define DATE_FORMAT_DEFAULT             2

#define DAY_BLANKING_MIN                0
#define DAY_BLANKING_NEVER              0  // Don't blank ever (default)
#define DAY_BLANKING_WEEKEND            1  // Blank during the weekend
#define DAY_BLANKING_WEEKDAY            2  // Blank during weekdays
#define DAY_BLANKING_ALWAYS             3  // Always blank
#define DAY_BLANKING_HOURS              4  // Blank between start and end hour every day
#define DAY_BLANKING_WEEKEND_OR_HOURS   5  // Blank between start and end hour during the week AND all day on the weekend
#define DAY_BLANKING_WEEKDAY_OR_HOURS   6  // Blank between start and end hour during the weekends AND all day on week days
#define DAY_BLANKING_WEEKEND_AND_HOURS  7  // Blank between start and end hour during the weekend
#define DAY_BLANKING_WEEKDAY_AND_HOURS  8  // Blank between start and end hour during week days
#define DAY_BLANKING_MAX                8
#define DAY_BLANKING_DEFAULT            0

#define BLANK_MODE_MIN                  0
#define BLANK_MODE_TUBES                0  // Use blanking for tubes only 
#define BLANK_MODE_LEDS                 1  // Use blanking for LEDs only
#define BLANK_MODE_BOTH                 2  // Use blanking for tubes and LEDs
#define BLANK_MODE_MAX                  2
#define BLANK_MODE_DEFAULT              2

#define BACKLIGHT_MIN                   0
#define BACKLIGHT_FIXED                 0   // Just define a colour and stick to it
#define BACKLIGHT_PULSE                 1   // pulse the defined colour
#define BACKLIGHT_CYCLE                 2   // cycle through random colours
#define BACKLIGHT_FIXED_DIM             3   // A defined colour, but dims with bulb dimming
#define BACKLIGHT_PULSE_DIM             4   // pulse the defined colour, dims with bulb dimming
#define BACKLIGHT_CYCLE_DIM             5   // cycle through random colours, dims with bulb dimming
#define BACKLIGHT_COLOUR_TIME           6   // use "ColourTime" - different colours for each digit value
#define BACKLIGHT_COLOUR_TIME_DIM       7   // use "ColourTime" - dims with bulb dimming
#define BACKLIGHT_MAX                   7
#define BACKLIGHT_DEFAULT               4

#define CYCLE_SPEED_MIN                 4
#define CYCLE_SPEED_MAX                 64
#define CYCLE_SPEED_DEFAULT             10

#define PIR_TIMEOUT_MIN                 60    // 1 minute in seconds
#define PIR_TIMEOUT_MAX                 3600  // 1 hour in seconds
#define PIR_TIMEOUT_DEFAULT             300   // 5 minutes in seconds

#define TEMP_DISPLAY_MODE_DUR_MS        5000
#define TIME_SET_MODE_DUR_MS            10000
#define SETTINGS_MODE_DUR_MS            60000

#define USE_LDR_DEFAULT                 true

#define SLOTS_MODE_MIN                  0
#define SLOTS_MODE_NONE                 0   // Don't use slots effect
#define SLOTS_MODE_1M_SCR_SCR           1   // use slots effect every minute, scroll in, scramble out
#define SLOTS_MODE_MAX                  1
#define SLOTS_MODE_DEFAULT              1

// RTC address
#define RTC_I2C_ADDRESS                 0x68

#define MAX_WIFI_TIME                   5

#define DO_NOT_APPLY_LEAD_0_BLANK       false
#define APPLY_LEAD_0_BLANK              true

#define H10                             5
#define H1                              4
#define M10                             3
#define M1                              2
#define S10                             1
#define S1                              0

//**********************************************************************************
//**********************************************************************************
//*                               Variables                                        *
//**********************************************************************************
//**********************************************************************************

// ***** Pin Defintions ****** Pin Defintions ****** Pin Defintions ******

// Shift register
#define LATCHPin    13    // Package pin 17 // PB5 // SCK
#define CLOCKPin    10    // Package pin 14 // PB2 // PWM - OC1B
#define DATA1Pin    8     // Package pin 12 // PB0 // MISO
#define BLANKPin    11    // Package pin 15 // PB3 // PWM - OC2A

// button input
#define inputPin1   A2    // Package pin 25 // PC2 // Analog or digital input for button.
#define inputPin2   1     // Package pin 31 // PD1

// PIR input
#define pirPin      A3    // Package pin 26 // PC3 // Analog or digital input for PIR

// PWM capable output for backlight - Neo Pixel chain
#define LED_DOUT    6     // package pin 12 // PD6

#define LDRPin      A1    // Package pin 24 // PC1 // Analog input for Light dependent resistor.

// Free GPIOs
#define GPIO1        0    // Package pin 30 // PD0 // RXD 
#define GPIO2        9    // Package pin 13 // PB1 // PWM capable - OC1A
#define GPIO3        A0   // Package pin 23 // PC0 // ADC input
#define GPIO4        ADC6 // Package pin 19 // ADC6 // ADC input
#define GPIO5        ADC7 // Package pin 22 // ADC7 // ADC input

// On board indicator LEDs
#define LEDA         4    // package pin 2 // PD4
#define LEDB         5    // package pin 9 // PD5 // PWM Capable

// ************** ISR ************ ISR ************ ISR ***************
volatile long long output;
volatile long long outputCurr;
volatile long long outputBuf;
volatile long long outputCurrBuf;
volatile long long lastOutput;
volatile byte phase = 0;
volatile byte switchTime = 0;
volatile byte switchTimeBuf = 0;

// 16 interrupts per impression - for fading
#define PHASE_MAX   16

//**********************************************************************************
Transition transition(500, 1000, 3000);

// ************************ Display management ************************
byte numberArray[DIGIT_COUNT]     = {0, 0, 0, 0, 0, 0};
byte currNumberArray[DIGIT_COUNT] = {0, 0, 0, 0, 0, 0};
byte displayType[DIGIT_COUNT]     = {NORMAL, NORMAL, NORMAL, NORMAL, NORMAL, NORMAL};
int fadeState                     = 0;
byte scrollCounter[DIGIT_COUNT]   = {0, 0, 0, 0, 0, 0};
byte ourIP[4]                     = {0, 0, 0, 0};       // set by the WiFi module
byte valueDisplayTime             = 0;
byte valueToShow[3]               = {0, 0, 0};
byte valueDisplayType[3]          = {0x33, 0x33, 0x33}; // All normal by default

// how many fade steps to increment (out of DIGIT_DISPLAY_COUNT) each impression
// 100 is about 1 second
int fadeStepsExternal = FADE_STEPS_DEFAULT;
int fadeStepsInternal = fadeStepsExternal / 10;

int scrollSteps = SCROLL_STEPS_DEFAULT;
bool scrollback = true;
bool fade = true;

// For software blinking
int blinkCounter = 0;
bool blinkState = true;

// leading digit blanking
bool blankLeading = false;

// Dimming value
int minDim = MIN_DIM_DEFAULT;
int ldrValue = 0;

unsigned int tempDisplayModeDuration;      // time for the end of the temporary display
int  tempDisplayMode;

int acpOffset = 0;        // Used to provide one arm bandit scolling
int acpTick = 0;          // The number of counts before we scroll
bool suppressACP = false;
byte slotsMode = SLOTS_MODE_DEFAULT;

byte currentMode = MODE_TIME;   // Initial cold start mode
byte nextMode = currentMode;
byte blankHourStart = 0;
byte blankHourEnd = 0;
byte blankMode = 0;
bool blankTubes = false;
bool blankLEDs = false;

// ************************ Ambient light dimming ************************
int dimDark = SENSOR_LOW_DEFAULT;
int dimBright = SENSOR_HIGH_DEFAULT;
double sensorLDRSmoothed = 0;
int sensorSmoothCountLDR = SENSOR_SMOOTH_READINGS_DEFAULT;
bool useLDR = true;
bool usePIRPullup = true;

// ************************ Clock variables ************************
// RTC, uses Analogue pins A4 (SDA) and A5 (SCL)
DS3231 Clock;

// State variables for detecting changes
byte lastSec;
unsigned long nowMillis = 0;
unsigned long lastCheckMillis = 0;

byte dateFormat = DATE_FORMAT_DEFAULT;
byte dayBlanking = DAY_BLANKING_DEFAULT;
bool blanked = false;
byte blankSuppressStep = 0;    // The press we are on: 1 press = suppress for 1 min, 2 press = 1 hour, 3 = 1 day
unsigned long blankSuppressedMillis = 0;   // The end time of the blanking, 0 if we are not suppressed
unsigned long blankSuppressedSelectionTimoutMillis = 0;   // Used for determining the end of the blanking period selection timeout
bool hourMode = false;

// PIR
unsigned long pirTimeout = PIR_TIMEOUT_DEFAULT;
unsigned long pirLastSeen = 0;
bool pirInstalled = false;
bool pirvalue = false;

bool triggeredThisSec = false;

byte useRTC = false;        // true if we detect an RTC
byte useWiFi = 0;           // the number of minutes ago we recevied information from the WiFi module, 0 = don't use WiFi
bool detectedWiFi = false;  // set true the first time we see a Wifi time update
bool resetWifi = false;     // set this true to make the WiFi module open the Access Point

// **************************** LED management ***************************
bool upOrDown;

// Blinking colons led in settings modes
int ledBlinkCtr = 0;
int ledBlinkNumber = 0;

byte backlightMode = BACKLIGHT_DEFAULT;

// Back light intensities
byte redCnl = COLOUR_RED_CNL_DEFAULT;
byte grnCnl = COLOUR_GRN_CNL_DEFAULT;
byte bluCnl = COLOUR_BLU_CNL_DEFAULT;
byte cycleCount = 0;
byte cycleSpeed = CYCLE_SPEED_DEFAULT;

int colors[3];

// individual channel colours for the LEDs
byte ledR[DIGIT_COUNT];
byte ledG[DIGIT_COUNT];
byte ledB[DIGIT_COUNT];

// set up the NeoPixel library
NeoPixelBus<NeoRgbFeature, NeoSk6812Method> leds(DIGIT_COUNT, LED_DOUT);

// Strategy 3
int changeSteps = 0;
byte currentColour = 0;

int impressionsPerSec = 0;
int lastImpressionsPerSec = 0;

// ********************** Input switch management **********************
ClockButton button1(inputPin1);
ClockButton button2(inputPin2);

// Simplified settings mode
bool settingsMode = false;       // tells us if we are past the initial 2 sec / 2 button push

// **************************** digit healing ****************************
// This is a special mode which repairs cathode poisoning by driving a
// single element at full power. To be used with care!
// In theory, this should not be necessary because we have ACP every 10mins,
// but some tubes just want to be poisoned
byte digitBurnDigit = 0;
byte digitBurnValue = 0;

//**********************************************************************************
//**********************************************************************************
//*                                    Setup                                       *
//**********************************************************************************
//**********************************************************************************
void setup()
{
  pinMode(LATCHPin, OUTPUT);
  pinMode(CLOCKPin, OUTPUT);
  pinMode(DATA1Pin, OUTPUT);
  pinMode(BLANKPin, OUTPUT);

  pinMode(LED_DOUT, OUTPUT);
  pinMode(pirPin, INPUT);

  pinMode(LEDA, OUTPUT);
  pinMode(LEDB, OUTPUT);  

  // The LEDS sometimes glow at startup, it annoys me, so turn them completely off
  setAllLEDs(0,0,0);

  // Set up the LED output
  leds.Begin();

  // Set up the PRNG with something so that it looks random
  randomSeed(analogRead(LDRPin));

  // Test if the button is pressed for factory reset
  for (int i = 0 ; i < 20 ; i++ ) {
    button1.checkButton(nowMillis);
  }

  // User requested factory reset
  if (button1.isButtonPressedNow()) {
    // Flash some ramdom c0lours to signal that we have accepted the factory reset
    for (int i = 0 ; i < 60 ; i++ ) {
      randomRGBFlash(20);
    }

    // mark that we have done the EEPROM setup
    EEPROM.write(EE_NEED_SETUP, true);
  }

  // If the button is held down while we are flashing, then do the test pattern
  bool doTestPattern = false;

  // Detect factory reset: button pressed on start or uninitialised EEPROM
  if (EEPROM.read(EE_NEED_SETUP)) {
    doTestPattern = true;
  }

  // Clear down any spurious button action
  button1.reset();
  button2.reset();

  // Test if the button is pressed for factory reset
  for (int i = 0 ; i < 20 ; i++ ) {
    button1.checkButton(nowMillis);
  }

  if (button1.isButtonPressedNow()) {
    doTestPattern = true;
  }

  // Read EEPROM values
  readEEPROMValues();

  // Not setting the pullup resistor allows us to use 3V3 devices (e.g. RCWL-0516)
  digitalWrite(pirPin, usePIRPullup); // set pull up resistor - if no PIR connected, default to "triggered"

  // Set up interrupt
  cli();
  //set timer1 interrupt at 1kHz
  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0

  // set timer count for 1kHz increments
  OCR1A = 1999;// = (16*10^6) / (1000*8) - 1

  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  
  // Set CS11 bit for 8 prescaler
  TCCR1B |= (1 << CS11);
   
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);
  
//  // turn on PWM output toggle
//  TCCR1A |= (1 << COM1B0);
//  
  sei();
 
  if (doTestPattern) {
    bool oldUseLDR = useLDR;
    byte oldBacklightMode = backlightMode;

    // reset the EEPROM values
    factoryReset();

    // turn off LDR
    useLDR = false;

    // turn off Scrollback
    scrollback = false;

    // set backlights to change with the displayed digits
    backlightMode = BACKLIGHT_COLOUR_TIME;

    // All the digits on full
    allNormal(DO_NOT_APPLY_LEAD_0_BLANK);

    int secCount = 0;
    lastCheckMillis = millis();

    bool inLoop = true;

    // We don't want to stay in test mode forever
    long startTestMode = lastCheckMillis;

    while (inLoop) {
      analogWrite(BLANKPin, 0);
      nowMillis = millis();
      if (nowMillis - lastCheckMillis > 1000) {
        lastCheckMillis = nowMillis;
        secCount++;
        secCount = secCount % 10;
      }

      loadNumberArraySameValue(secCount);
      outputDisplay();

      setLeds();
      button1.checkButton(nowMillis);
      if (button1.isButtonPressedNow() && (secCount == 8)) {
        inLoop = false;
      }

      bool ledStateA = (secCount % 2 == 0);
      digitalWrite(LEDA, ledStateA);
      getRTCTime();
      bool ledStateB = ((second() % 2 == 0) && (year() > 1999));
      digitalWrite(LEDB, ledStateB);
    }

    useLDR = oldUseLDR;
    backlightMode = oldBacklightMode;
  }

  // reset the LEDs
  setLeds();

  // Clear down any spurious button action
  button1.reset();

  // initialise the internal time (in case we don't find the time provider)
  nowMillis = millis();
  setTime(12, 34, 56, 1, 3, 2019);
  getRTCTime();

  // Show the version for 1 s
  tempDisplayMode = TEMP_MODE_VERSION;
  tempDisplayModeDuration = TEMP_DISPLAY_MODE_DUR_MS;

  // don't blank anything right now
  blanked = false;
  setTubesAndLEDSBlankMode();

  // mark that we have done the EEPROM setup
  EEPROM.write(EE_NEED_SETUP, false);

  // Turn off LEDs
  digitalWrite(LEDA, LOW);
  digitalWrite(LEDB, LOW);
      
  // enable watchdog
  wdt_enable(WDTO_8S);
}

// ************************************************************
// Perform the shift out to the shift register
// #define LATCHPin    13    // Package pin 17 // PB5 // SCK
// #define CLOCKPin    10    // Package pin 14 // PB2 // PWM - OC1B
// #define DATA1Pin    8     // Package pin 12 // PB0 // MISO
// #define BLANKPin    11    // Package pin 15 // PB3 // PWM - OC2A
// ************************************************************
void shiftOut64(long long _val1) {
  uint8_t i, j;
  long long numberToShift = _val1;

  PORTB = PORTB & B11011111;

  byte shiftBytes[8];

  memcpy(&shiftBytes[0], &_val1, 8);

  for (j = 0; j < 8; j++) {
    byte byteToWorkOn = shiftBytes[7-j];
    for (i = 0; i < 8; i++) {
      if (byteToWorkOn & B10000000 >> i) {
        PORTB = PORTB | B00000001;
      } else {
        PORTB = PORTB & B11111110;
      }
      PORTB = PORTB | B00000100;
      PORTB = PORTB & B11111011;
    }
  }
  
  PORTB = PORTB | B00100000;
}

// ************************************************************
// Display interrupt routine
// ************************************************************
ISR(TIMER1_COMPA_vect) {
  phase++;
  if (phase == PHASE_MAX) {
    // reload the buffers
    output = outputBuf;
    outputCurr = outputCurrBuf;
    switchTime = switchTimeBuf;

    // reset the phase counter (for fading)
    phase = 0;
  }

  if (phase == switchTime) {
    // Switch the display value if it has changed
    if (output != lastOutput) {
      shiftOut64(output);
      lastOutput = output;
    }      
  } else if (phase == 0) {
    // Output the display value if it has changed
    if (outputCurr != lastOutput) {
      shiftOut64(outputCurr);
      lastOutput = outputCurr;
    }
  }  

  
  // Deal with blink, calculate if we are on or off
  blinkCounter++;
  if (blinkCounter == BLINK_COUNT_MAX) {
    blinkCounter = 0;
    blinkState = !blinkState;
  }
}

//**********************************************************************************
//**********************************************************************************
//*                              Main loop                                         *
//**********************************************************************************
//**********************************************************************************
bool millisNotSet = true;
void loop()
{
  nowMillis = millis();

  // shows us how fast the inner loop is running
  impressionsPerSec++;

  // -------------------------------------------------------------------------------
  
  if (abs(nowMillis - lastCheckMillis) >= 1000) {
    if ((second() == 0) && (!triggeredThisSec)) {
      if ((minute() == 0)) {
        if (hour() == 0) {
          performOncePerDayProcessing();
        }

        performOncePerHourProcessing();
      }

      performOncePerMinuteProcessing();
    }

    performOncePerSecondProcessing();

    // Make sure we don't call multiple times
    triggeredThisSec = true;

    if ((second() > 0) && triggeredThisSec) {
      triggeredThisSec = false;
    }

    lastCheckMillis = nowMillis;
  }
  
  // -------------------------------------------------------------------------------

  // Check button, we evaluate below
  button1.checkButton(nowMillis);
  button2.checkButton(nowMillis);

  // ----------------------- Hours and Minutes Setting -----------------------------

  if (!settingsMode) {
    bool but1 = button1.isButtonPressed1S();
    bool but2 = button2.isButtonPressed1S();
    
    if (but1 && but2) {
      settingsMode = true;
      allNormal(DO_NOT_APPLY_LEAD_0_BLANK);
      nextMode = currentMode = MODE_SECS_SET;
      tempDisplayModeDuration = SETTINGS_MODE_DUR_MS;
      button1.inhibit(nowMillis, 1000);
      button2.inhibit(nowMillis, 1000);
    } else if (but1) {
      nextMode = currentMode = MODE_HOURS_SET;
      tempDisplayModeDuration = TIME_SET_MODE_DUR_MS;
    } else if (but2) {
      tempDisplayModeDuration = TIME_SET_MODE_DUR_MS;
      nextMode = currentMode = MODE_MINS_SET;
    }    
  }

  // ----------------------------- Settings Modes ----------------------------------
  
  if (settingsMode) {
    if (button1.isButtonPressedAndReleased()) {
      // Move to the next setting
      currentMode = nextMode;
      tempDisplayModeDuration = SETTINGS_MODE_DUR_MS;
    } else if (button1.isButtonPressed()) {
      // Preview the next setting
      nextMode = currentMode + 1;
  
      if (nextMode > MODE_MAX) {
        nextMode = MODE_SECS_SET;
      }
    }

    // ***Exit*** settings mode with save
    if (button1.isButtonPressed2S() || button2.isButtonPressed2S()) {
      currentMode = MODE_MIN;
  
      // Store the EEPROM if we exit the config mode
      saveEEPROMValues();
  
      // Preset the display
      allNormal(APPLY_LEAD_0_BLANK);
  
      nextMode = currentMode;
  
      settingsMode = false;

      tempDisplayModeDuration = 0;
      
      button1.inhibit(nowMillis, 1000);
      button2.inhibit(nowMillis, 1000);
    }

    // ***Exit*** settings mode without save on timeout
    if (tempDisplayModeDuration == 0) {
      // ***Exit*** settings mode
      currentMode = MODE_MIN;
  
      // Preset the display
      allNormal(APPLY_LEAD_0_BLANK);
  
      nextMode = currentMode;
  
      settingsMode = false;      
    }   
  }

  // -------------------------------------------------------------------------------
  
  // ************* Process the modes *************
  if (nextMode != currentMode) {
    setNextMode(nextMode);
  } else {
    processCurrentMode(currentMode);
  }

  // get the LDR ambient light reading
  int blankPWMValue = 255 - ((float) getDimmingFromLDR() * 255.0 / 1000.0);
  analogWrite(BLANKPin, blankPWMValue);
  
  // One armed bandit trigger every 10th minute
  if (acpOffset == 0) {
    if (((minute() % 10) == 9) && (second() == 15)) {
      // suppress ACP when fully dimmed
      if (suppressACP) {
        if (ldrValue > minDim) {
          acpOffset = 1;
        }
      } else {
        acpOffset = 1;
      }
    }
  }

  // One armed bandit handling
  if (acpOffset > 0) {
    if (acpTick >= acpOffset) {
      acpTick = 0;
      acpOffset++;
      if (acpOffset == 50) {
        acpOffset = 0;
      }
    } else {
      acpTick++;
    }
  }

  // get the LDR ambient light reading
  ldrValue = getDimmingFromLDR();

  // Set normal output display
  outputDisplay();

  // Prepare the tick and backlight LEDs
  setLeds();

  delay(10);
}

// ************************************************************
// Called once per second
// ************************************************************
void performOncePerSecondProcessing() {

  // Store the current value and reset
  lastImpressionsPerSec = impressionsPerSec;
  impressionsPerSec = 0;

  // Change the direction of the pulse
  upOrDown = !upOrDown;

  // If we are in temp display mode, decrement the count
  if (tempDisplayModeDuration > 0) {
    if (tempDisplayModeDuration > 1000) {
      tempDisplayModeDuration -= 1000;
    } else {
      tempDisplayModeDuration = 0;
    }
  }

  // decrement the blanking supression counter
  if (blankSuppressedMillis > 0) {
    if (blankSuppressedMillis > 1000) {
      blankSuppressedMillis -= 1000;
    } else {
      blankSuppressedMillis = 0;
    }
  }

  // Get the blanking status, this may be overridden by blanking suppression
  // Only blank if we are in TIME mode
  if (currentMode == MODE_TIME) {
    bool pirBlanked = checkPIR(nowMillis);
    bool nativeBlanked = checkBlanking();

    // Check if we are in blanking suppression mode
    blanked = (nativeBlanked || pirBlanked) && (blankSuppressedMillis == 0);

    // reset the blanking period selection timer
    if (nowMillis > blankSuppressedSelectionTimoutMillis) {
      blankSuppressedSelectionTimoutMillis = 0;
      blankSuppressStep = 0;
    }
  } else {
    blanked = false;
  }
  setTubesAndLEDSBlankMode();

  // Decrement the value display counter
  // 0xff means "forever", so don't decrement it
  if ((valueDisplayTime > 0) && (valueDisplayTime < 0xff)) {
    valueDisplayTime--;
  }

  // sync the blink with the seconds
  if (blinkCounter > 10) blinkCounter--;

  // If we used to have a WiFi, but don't see it now, light LEDB
  digitalWrite(LEDA, detectedWiFi && (useWiFi == 0));

  // If we see the PIR right now, light the LED dimly
  if (pirInstalled && pirvalue) {
    analogWrite(LEDB, 5);
  } else {
    analogWrite(LEDB, 0);
  }

  // feed the watchdog
  wdt_reset();
}

// ************************************************************
// Called once per minute
// ************************************************************
void performOncePerMinuteProcessing() {
  if (useWiFi > 0) {
    if (useWiFi == MAX_WIFI_TIME) {
      // We recently got an update, send to the RTC (if installed)
      setRTC();
      
      if(!detectedWiFi) {
        detectedWiFi = true;
      }
    }
    useWiFi--;
  } else {
    // get the time from the external RTC provider - (if installed)
    getRTCTime();
  }

  // AM/PM indicator - show if we are in 12H mode and it is PM
  // dpArray[1] = hourMode && (hour() > 12);
}

// ************************************************************
// Called once per hour
// ************************************************************
void performOncePerHourProcessing() {
}

// ************************************************************
// Called once per day
// ************************************************************
void performOncePerDayProcessing() {
}

// ************************************************************
// Set the seconds tick led(s) and the back lights
// ************************************************************
void setLeds()
{
  int secsTriangle;
  if (upOrDown) {
    secsTriangle = (nowMillis - lastCheckMillis);
  } else {
    secsTriangle = 1000 - (nowMillis - lastCheckMillis);
  }

  int secsSawtooth = nowMillis - lastCheckMillis;

  // calculate the PWM factor, goes between minDim% and 100%
  float dimFactor = (float) ldrValue / (float) MAX_DIM;
  float pwmFactor = (float) secsTriangle / (float) 1000.0;

  if (blankLEDs) {
          setAllLEDs( getLEDAdjusted(0, 1, 1), 
                      getLEDAdjusted(0, 1, 1), 
                      getLEDAdjusted(0, 1, 1));
  } else {
    // RGB Backlight PWM led output
    if (currentMode == MODE_TIME) {
      switch (backlightMode) {
        case BACKLIGHT_FIXED:
          setAllLEDs( getLEDAdjusted(rgb_backlight_curve[redCnl], 1, 1), 
                      getLEDAdjusted(rgb_backlight_curve[grnCnl], 1, 1), 
                      getLEDAdjusted(rgb_backlight_curve[bluCnl], 1, 1));
          break;
        case BACKLIGHT_PULSE:
          setAllLEDs( getLEDAdjusted(rgb_backlight_curve[redCnl], pwmFactor, 1),
                      getLEDAdjusted(rgb_backlight_curve[grnCnl], pwmFactor, 1),
                      getLEDAdjusted(rgb_backlight_curve[bluCnl], pwmFactor, 1));
          break;
        case BACKLIGHT_CYCLE:
          cycleColours3(colors);
          setAllLEDs( getLEDAdjusted(colors[0], 1, 1),
                      getLEDAdjusted(colors[1], 1, 1),
                      getLEDAdjusted(colors[2], 1, 1));
          break;
        case BACKLIGHT_FIXED_DIM:
          setAllLEDs( getLEDAdjusted(rgb_backlight_curve[redCnl], 1, dimFactor), 
                      getLEDAdjusted(rgb_backlight_curve[grnCnl], 1, dimFactor), 
                      getLEDAdjusted(rgb_backlight_curve[bluCnl], 1, dimFactor));
          break;
        case BACKLIGHT_PULSE_DIM:
          setAllLEDs( getLEDAdjusted(rgb_backlight_curve[redCnl], pwmFactor, dimFactor),
                      getLEDAdjusted(rgb_backlight_curve[grnCnl], pwmFactor, dimFactor),
                      getLEDAdjusted(rgb_backlight_curve[bluCnl], pwmFactor, dimFactor));
          break;
        case BACKLIGHT_CYCLE_DIM:
          cycleColours3(colors);
          setAllLEDs( getLEDAdjusted(colors[0], 1, dimFactor),
                      getLEDAdjusted(colors[1], 1, dimFactor),
                      getLEDAdjusted(colors[2], 1, dimFactor));
          break;
        case BACKLIGHT_COLOUR_TIME:
            for (int i = 0 ; i < DIGIT_COUNT ; i++) {
              ledR[i] = getLEDAdjusted(colourTimeR[numberArray[i]],1,1);
              ledG[i] = getLEDAdjusted(colourTimeG[numberArray[i]],1,1);
              ledB[i] = getLEDAdjusted(colourTimeB[numberArray[i]],1,1);
            }
            outputLEDBuffer();
          break;
        case BACKLIGHT_COLOUR_TIME_DIM:
            for (int i = 0 ; i < DIGIT_COUNT ; i++) {
              ledR[i] = getLEDAdjusted(colourTimeR[numberArray[i]],1,dimFactor);
              ledG[i] = getLEDAdjusted(colourTimeG[numberArray[i]],1,dimFactor);
              ledB[i] = getLEDAdjusted(colourTimeB[numberArray[i]],1,dimFactor);
            }
            outputLEDBuffer();
          break;
      }
    } else {
      if (currentMode == MODE_HOURS_SET) {
        for (int i = 0 ; i < DIGIT_COUNT ; i++) {
          ledR[i] = 0;
          ledG[i] = 0;
          ledB[i] = 0;
        }
        if (secsSawtooth > 500) {
          ledG[H10] = 255;
          ledG[H1] = 255;
        }
        outputLEDBuffer();
      } else if (currentMode == MODE_MINS_SET) {
        for (int i = 0 ; i < DIGIT_COUNT ; i++) {
          ledR[i] = 0;
          ledG[i] = 0;
          ledB[i] = 0;
        }
        if (secsSawtooth > 500) {
          ledG[M10] = 255;
          ledG[M1] = 255;
        }
        outputLEDBuffer();
      } else if (settingsMode) {
        if (secsSawtooth < 500) {
          switch(random(3)) {
            case 0:
              setAllLEDs(255,0,0);
              break;
            case 1:
              setAllLEDs(0,255,0);
              break;
            case 2:
              setAllLEDs(0,0,255);
              break;
          }
        } else {
          setAllLEDs(0,0,0);
        }
        outputLEDBuffer();
      }
    }
  }
}

// ************************************************************
// Check the PIR status. If we don't have a PIR installed, we
// don't want to respect the pin value, because it would defeat
// normal day blanking. The first time the PIR takes the pin low
// we mark that we have a PIR and we should start to respect
// the sensor.
// Returns true if PIR sensor is installed and we are blanked
// ************************************************************
bool checkPIR(unsigned long nowMillis) {
  pirvalue = (digitalRead(pirPin) == HIGH);

  if (pirvalue) {
    pirLastSeen = nowMillis;
    return false;
  } else {
    // Note that we have a pir
    pirInstalled = true;    
    if (nowMillis > (pirLastSeen + (pirTimeout * 1000))) {
      return true;
    } else {
      return false;
    }
  }
}

// ************************************************************
// Set back light LEDs to the same colour
// ************************************************************
void setAllLEDs(byte red, byte green, byte blue) {
  for (int i = 0 ; i < DIGIT_COUNT ; i++) {
    ledR[i] = red;
    ledG[i] = green;
    ledB[i] = blue;
  }
  outputLEDBuffer();
}

// ************************************************************
// Put the led buffers out
// ************************************************************
void outputLEDBuffer() {
  for (int i = 0 ; i < DIGIT_COUNT ; i++) {

#ifdef NORMAL_DISPLAY
    leds.SetPixelColor(i, RgbColor(ledR[DIGIT_COUNT - 1 - i], ledG[DIGIT_COUNT - 1 - i], ledB[DIGIT_COUNT - 1 - i]));
#else
    leds.SetPixelColor(i, RgbColor(ledR[i], ledG[i], ledB[i]));
#endif
  }
  leds.Show();
}

// ************************************************************
// Show the preview of the next mode - we stay in this mode until the 
// button is released
// ************************************************************
void setNextMode(int displayMode) {
  // turn off blanking
  blanked = false;
  setTubesAndLEDSBlankMode();

  switch (displayMode) {
    case MODE_TIME: {
        loadNumberArrayTime();
        allNormal(APPLY_LEAD_0_BLANK);
        break;
      }
    case MODE_HOURS_SET: {
        if (useWiFi > 0) {
          setNewNextMode(MODE_12_24);
        }
        loadNumberArrayTime();
        highlightHrs();
        break;
      }
    case MODE_MINS_SET: {
        loadNumberArrayTime();
        highlightMins();
        break;
      }
    case MODE_SECS_SET: {
        loadNumberArrayTime();
        highlightSecs();
        break;
      }
    case MODE_DAYS_SET: {
        loadNumberArrayDate();
        highlightDaysDateFormat();
        break;
      }
    case MODE_MONTHS_SET: {
        loadNumberArrayDate();
        highlightMonthsDateFormat();
        break;
      }
    case MODE_YEARS_SET: {
        loadNumberArrayDate();
        highlightYearsDateFormat();
        break;
      }
    case MODE_12_24: {
        loadNumberArrayConfBool(hourMode, displayMode);
        displayConfig();
        break;
      }
    case MODE_LEAD_BLANK: {
        loadNumberArrayConfBool(blankLeading, displayMode);
        displayConfig();
        break;
      }
    case MODE_SCROLLBACK: {
        loadNumberArrayConfBool(scrollback, displayMode);
        displayConfig();
        break;
      }
    case MODE_FADE: {
        loadNumberArrayConfBool(fade, displayMode);
        displayConfig();
        break;
      }
    case MODE_DATE_FORMAT: {
        loadNumberArrayConfInt(dateFormat, displayMode);
        displayConfig();
        break;
      }
    case MODE_DAY_BLANKING: {
        loadNumberArrayConfInt(dayBlanking, displayMode);
        displayConfig();
        break;
      }
    case MODE_HR_BLNK_START: {
        if ((dayBlanking == DAY_BLANKING_NEVER) ||
            (dayBlanking == DAY_BLANKING_WEEKEND) ||
            (dayBlanking == DAY_BLANKING_WEEKDAY) ||
            (dayBlanking == DAY_BLANKING_ALWAYS)) {
          // Skip past the start and end hour if the blanking mode says it is not relevant
          setNewNextMode(MODE_SUPPRESS_ACP);
        }
        loadNumberArrayConfInt(blankHourStart, displayMode);
        displayConfig();
        break;
      }
    case MODE_HR_BLNK_END: {
        loadNumberArrayConfInt(blankHourEnd, displayMode);
        displayConfig();
        break;
      }
    case MODE_SUPPRESS_ACP: {
        loadNumberArrayConfBool(suppressACP, displayMode);
        displayConfig();
        break;
      }
    case MODE_USE_LDR: {
        loadNumberArrayConfBool(useLDR, displayMode);
        displayConfig();
        break;
      }
    case MODE_BLANK_MODE: {
        loadNumberArrayConfInt(blankMode, displayMode);
        displayConfig();
        break;
      }
    case MODE_FADE_STEPS_UP:
    case MODE_FADE_STEPS_DOWN: {
        loadNumberArrayConfInt(fadeStepsExternal, displayMode);
        displayConfig();
        break;
      }
    case MODE_DISPLAY_SCROLL_STEPS_UP:
    case MODE_DISPLAY_SCROLL_STEPS_DOWN: {
        loadNumberArrayConfInt(scrollSteps, displayMode);
        displayConfig();
        break;
      }
    case MODE_PIR_TIMEOUT_UP:
    case MODE_PIR_TIMEOUT_DOWN: {
        loadNumberArrayConfInt(pirTimeout, displayMode);
        displayConfig();
        break;
      }
    case MODE_SLOTS_MODE: {
        loadNumberArrayConfInt(slotsMode, displayMode);
        displayConfig();
        break;
      }
    case MODE_BACKLIGHT_MODE: {
        loadNumberArrayConfInt(backlightMode, displayMode);
        displayConfig();
        break;
      }
    case MODE_RED_CNL: {
        if ((backlightMode == BACKLIGHT_CYCLE) || (backlightMode == BACKLIGHT_CYCLE_DIM))  {
          // Skip if we are in cycle mode
          setNewNextMode(MODE_CYCLE_SPEED);
        }
        loadNumberArrayConfInt(redCnl, displayMode);
        displayConfig();
        break;
      }
    case MODE_GRN_CNL: {
        loadNumberArrayConfInt(grnCnl, displayMode);
        displayConfig();
        break;
      }
    case MODE_BLU_CNL: {
        loadNumberArrayConfInt(bluCnl, displayMode);
        displayConfig();
        break;
      }
    case MODE_CYCLE_SPEED: {
        if ((backlightMode != BACKLIGHT_CYCLE) && (backlightMode != BACKLIGHT_CYCLE_DIM))  {
          // Show only if we are in cycle mode
          setNewNextMode(MODE_MIN_DIM_UP);
        }
        loadNumberArrayConfInt(cycleSpeed, displayMode);
        displayConfig();
        break;
      }
    case MODE_MIN_DIM_UP:
    case MODE_MIN_DIM_DOWN: {
        loadNumberArrayConfInt(minDim, displayMode);
        displayConfig();
        break;
      }
    case MODE_USE_PIR_PULLUP: {
        loadNumberArrayConfBool(usePIRPullup, displayMode);
        displayConfig();
        break;
      }
    case MODE_TEMP: {
        loadNumberArrayTemp(displayMode);
        displayConfig();
        break;
      }
    case MODE_VERSION: {
        loadNumberArrayConfInt(SOFTWARE_VERSION, displayMode);
        displayConfig();
        break;
      }
    case MODE_TUBE_TEST: {
        loadNumberArrayTestDigits();
        allNormal(DO_NOT_APPLY_LEAD_0_BLANK);
        break;
      }
  }
}

// ************************************************************
// Show the next mode - once the button is released
// ************************************************************
void processCurrentMode(int displayMode) {
  switch (displayMode) {
    case MODE_TIME: {
        if (button1.isButtonPressedAndReleased()) {
          // Deal with blanking first
          if ((nowMillis < blankSuppressedSelectionTimoutMillis) || blanked) {
            if (blankSuppressedSelectionTimoutMillis == 0) {
              // Apply 5 sec timeout for setting the suppression time - pressing the
              // button multiple times in this period increases the blank suppression
              // timeout
              blankSuppressedSelectionTimoutMillis = nowMillis + TEMP_DISPLAY_MODE_DUR_MS;
            }

            blankSuppressStep++;
            if (blankSuppressStep > 3) {
              blankSuppressStep = 3;
            }

            if (blankSuppressStep == 1) {
              blankSuppressedMillis = 10000;
            } else if (blankSuppressStep == 2) {
              blankSuppressedMillis = 3600000;
            } else if (blankSuppressStep == 3) {
              blankSuppressedMillis = 3600000 * 4;
            }
            blanked = false;
            setTubesAndLEDSBlankMode();
          } else {
            // Always start from the first mode, or increment the temp mode if we are already in a display
            if (tempDisplayModeDuration > 0) {
              tempDisplayModeDuration = TEMP_DISPLAY_MODE_DUR_MS;
              tempDisplayMode++;
            } else {
              tempDisplayMode = TEMP_MODE_MIN;
            }

            if (tempDisplayMode > TEMP_MODE_MAX) {
              tempDisplayMode = TEMP_MODE_MIN;
            }

            tempDisplayModeDuration = TEMP_DISPLAY_MODE_DUR_MS;
          }
        }

        if (tempDisplayModeDuration > 0) {
          blanked = false;
          setTubesAndLEDSBlankMode();
          if (tempDisplayMode == TEMP_MODE_DATE) {
            loadNumberArrayDate();
          }

          if (tempDisplayMode == TEMP_MODE_TEMP) {
            byte timeProv = 10;
            if (useRTC) timeProv += 1;
            if (useWiFi > 0) timeProv += 2;
            loadNumberArrayTemp(timeProv);
          }

          if (tempDisplayMode == TEMP_MODE_LDR) {
            loadNumberArrayLDR();
          }

          if (tempDisplayMode == TEMP_MODE_VERSION) {
            loadNumberArrayConfInt(SOFTWARE_VERSION, 0);
          }

          if (tempDisplayMode == TEMP_IP_ADDR12) {
            if (useWiFi > 0) {
              loadNumberArrayIP(ourIP[0], ourIP[1]);
            } else {
              // we can't show the IP address if we have the RTC, just skip
              tempDisplayMode++;
            }
          }

          if (tempDisplayMode == TEMP_IP_ADDR34) {
            if (useWiFi > 0) {
              loadNumberArrayIP(ourIP[2], ourIP[3]);
            } else {
              // we can't show the IP address if we have the RTC, just skip
              tempDisplayMode++;
            }
          }

          if (tempDisplayMode == TEMP_IMPR) {
            loadNumberArrayConfInt(lastImpressionsPerSec, 0);
          }

          allNormal(DO_NOT_APPLY_LEAD_0_BLANK);
        } else {
          if (acpOffset > 0) {
            loadNumberArrayACP();
            allNormal(DO_NOT_APPLY_LEAD_0_BLANK);
            // ToDo set max brigtness
          } else {
            if (slotsMode > SLOTS_MODE_MIN) {
              if (second() == 50) {

                // initialise the slots values
                loadNumberArrayDate();
                transition.setAlternateValues();
                loadNumberArrayTime();
                transition.setRegularValues();
                allNormal(DO_NOT_APPLY_LEAD_0_BLANK);

                transition.start(nowMillis);
              }

              // initialise the slots mode
              bool msgDisplaying;
              switch (slotsMode) {
                case SLOTS_MODE_1M_SCR_SCR:
                {
                  msgDisplaying = transition.scrollInScrambleOut(nowMillis);
                  break;
                }
              }

              // Continue slots
              if (!msgDisplaying) {
                // do normal time thing when we are not in slots
                loadNumberArrayTime();

                allNormal(APPLY_LEAD_0_BLANK);
              }
            } else {
              // no slots mode, just do normal time thing
              loadNumberArrayTime();

              allNormal(APPLY_LEAD_0_BLANK);
            }
          }
        }
        break;
      }
    case MODE_MINS_SET: {
        if (tempDisplayModeDuration > 0) {
          if (button1.isButtonPressedAndReleased()) {
            incMins();

            // extend the time set mode
            tempDisplayModeDuration = TIME_SET_MODE_DUR_MS;
          }
          
          if (button2.isButtonPressedAndReleased()) {
            decMins();

            // extend the time set mode
            tempDisplayModeDuration = TIME_SET_MODE_DUR_MS;
          }
          loadNumberArrayTime();
          highlightMins();
        } else {
          // Exit set mode on timeout
          currentMode = nextMode = MODE_MIN;
        }
        break;
      }
    case MODE_HOURS_SET: {
        if (tempDisplayModeDuration > 0) {
          if (button1.isButtonPressedAndReleased()) {
            incHours();

            // extend the time set mode
            tempDisplayModeDuration = TIME_SET_MODE_DUR_MS;
          }
          if (button2.isButtonPressedAndReleased()) {
            decHours();
            
            // extend the time set mode
            tempDisplayModeDuration = TIME_SET_MODE_DUR_MS;
          }
          loadNumberArrayTime();
          highlightHrs();
        } else {
          // Exit set mode on timeout
          currentMode = nextMode = MODE_MIN;
        }
        break;
      }
    case MODE_SECS_SET: {
        if (button2.isButtonPressedAndReleased()) {
          resetSecond();
        }
        loadNumberArrayTime();
        highlightSecs();
        break;
      }
    case MODE_DAYS_SET: {
        if (button2.isButtonPressedAndReleased()) {
          incDays();
        }
        loadNumberArrayDate();
        highlightDaysDateFormat();
        break;
      }
    case MODE_MONTHS_SET: {
        if (button2.isButtonPressedAndReleased()) {
          incMonths();
        }
        loadNumberArrayDate();
        highlightMonthsDateFormat();
        break;
      }
    case MODE_YEARS_SET: {
        if (button2.isButtonPressedAndReleased()) {
          incYears();
        }
        loadNumberArrayDate();
        highlightYearsDateFormat();
        break;
      }
    case MODE_12_24: {
        if (button2.isButtonPressedAndReleased()) {
          hourMode = ! hourMode;
        }
        loadNumberArrayConfBool(hourMode, displayMode);
        displayConfig();
        break;
      }
    case MODE_LEAD_BLANK: {
        if (button2.isButtonPressedAndReleased()) {
          blankLeading = !blankLeading;
        }
        loadNumberArrayConfBool(blankLeading, displayMode);
        displayConfig();
        break;
      }
    case MODE_SCROLLBACK: {
        if (button2.isButtonPressedAndReleased()) {
          scrollback = !scrollback;
        }
        loadNumberArrayConfBool(scrollback, displayMode);
        displayConfig();
        break;
      }
    case MODE_FADE: {
        if (button2.isButtonPressedAndReleased()) {
          fade = !fade;
        }
        loadNumberArrayConfBool(fade, displayMode);
        displayConfig();
        break;
      }
    case MODE_DATE_FORMAT: {
        if (button2.isButtonPressedAndReleased()) {
          dateFormat++;
          if (dateFormat > DATE_FORMAT_MAX) {
            dateFormat = DATE_FORMAT_MIN;
          }
        }
        loadNumberArrayConfInt(dateFormat, displayMode);
        displayConfig();
        break;
      }
    case MODE_DAY_BLANKING: {
        if (button2.isButtonPressedAndReleased()) {
          dayBlanking++;
          if (dayBlanking > DAY_BLANKING_MAX) {
            dayBlanking = DAY_BLANKING_MIN;
          }
        }
        loadNumberArrayConfInt(dayBlanking, displayMode);
        displayConfig();
        break;
      }
    case MODE_HR_BLNK_START: {
        if (button2.isButtonPressedAndReleased()) {
          blankHourStart++;
          if (blankHourStart > HOURS_MAX) {
            blankHourStart = 0;
          }
        }
        loadNumberArrayConfInt(blankHourStart, displayMode);
        displayConfig();
        break;
      }
    case MODE_HR_BLNK_END: {
        if (button2.isButtonPressedAndReleased()) {
          blankHourEnd++;
          if (blankHourEnd > HOURS_MAX) {
            blankHourEnd = 0;
          }
        }
        loadNumberArrayConfInt(blankHourEnd, displayMode);
        displayConfig();
        break;
      }
    case MODE_SUPPRESS_ACP: {
        if (button2.isButtonPressedAndReleased()) {
          suppressACP = !suppressACP;
        }
        loadNumberArrayConfBool(suppressACP, displayMode);
        displayConfig();
        break;
      }
    case MODE_BLANK_MODE: {
        if (button2.isButtonPressedAndReleased()) {
          blankMode++;
          if (blankMode > BLANK_MODE_MAX) {
            blankMode = BLANK_MODE_MIN;
          }
        }
        loadNumberArrayConfInt(blankMode, displayMode);
        displayConfig();
        break;
      }
    case MODE_USE_LDR: {
        if (button2.isButtonPressedAndReleased()) {
          useLDR = !useLDR;
        }
        loadNumberArrayConfBool(useLDR, displayMode);
        displayConfig();
        break;
      }
    case MODE_FADE_STEPS_UP: {
        if (button2.isButtonPressedAndReleased()) {
          fadeStepsExternal++;
          if (fadeStepsExternal > FADE_STEPS_MAX) {
            fadeStepsExternal = FADE_STEPS_MIN;
          }
        }
        loadNumberArrayConfInt(fadeStepsExternal, displayMode);
        displayConfig();
        break;
      }
    case MODE_FADE_STEPS_DOWN: {
        if (button2.isButtonPressedAndReleased()) {
          fadeStepsExternal--;
          if (fadeStepsExternal < FADE_STEPS_MIN) {
            fadeStepsExternal = FADE_STEPS_MAX;
          }
          fadeStepsInternal = fadeStepsExternal;
        }
        loadNumberArrayConfInt(fadeStepsExternal, displayMode);
        displayConfig();
        break;
      }
    case MODE_DISPLAY_SCROLL_STEPS_UP: {
        if (button2.isButtonPressedAndReleased()) {
          scrollSteps++;
          if (scrollSteps > SCROLL_STEPS_MAX) {
            scrollSteps = SCROLL_STEPS_MIN;
          }
        }
        loadNumberArrayConfInt(scrollSteps, displayMode);
        displayConfig();
        break;
      }
    case MODE_DISPLAY_SCROLL_STEPS_DOWN: {
        if (button2.isButtonPressedAndReleased()) {
          scrollSteps--;
          if (scrollSteps < SCROLL_STEPS_MIN) {
            scrollSteps = SCROLL_STEPS_MAX;
          }
        }
        loadNumberArrayConfInt(scrollSteps, displayMode);
        displayConfig();
        break;
      }
    case MODE_PIR_TIMEOUT_UP: {
        if (button2.isButtonPressedAndReleased()) {
          pirTimeout+=10;
          if (pirTimeout > PIR_TIMEOUT_MAX) {
            pirTimeout = PIR_TIMEOUT_MIN;
          }
        }
        loadNumberArrayConfInt(pirTimeout, displayMode);
        displayConfig();
        break;
      }
    case MODE_PIR_TIMEOUT_DOWN: {
        if (button2.isButtonPressedAndReleased()) {
          pirTimeout-=10;
          if (pirTimeout < PIR_TIMEOUT_MIN) {
            pirTimeout = PIR_TIMEOUT_MAX;
          }
        }
        loadNumberArrayConfInt(pirTimeout, displayMode);
        displayConfig();
        break;
      }
    case MODE_SLOTS_MODE: {
        if (button2.isButtonPressedAndReleased()) {
          slotsMode++;
          if (slotsMode > SLOTS_MODE_MAX) {
            slotsMode = SLOTS_MODE_MIN;
          }
        }
        loadNumberArrayConfInt(slotsMode, displayMode);
        displayConfig();
        break;
      }
    case MODE_BACKLIGHT_MODE: {
        if (button2.isButtonPressedAndReleased()) {
          backlightMode++;
          if (backlightMode > BACKLIGHT_MAX) {
            backlightMode = BACKLIGHT_MIN;
          }
        }
        loadNumberArrayConfInt(backlightMode, displayMode);
        displayConfig();
        break;
      }
    case MODE_RED_CNL: {
        if (button2.isButtonPressedAndReleased()) {
          redCnl++;
          if (redCnl > COLOUR_CNL_MAX) {
            redCnl = COLOUR_CNL_MIN;
          }
        }
        loadNumberArrayConfInt(redCnl, displayMode);
        displayConfig();
        break;
      }
    case MODE_GRN_CNL: {
        if (button2.isButtonPressedAndReleased()) {
          grnCnl++;
          if (grnCnl > COLOUR_CNL_MAX) {
            grnCnl = COLOUR_CNL_MIN;
          }
        }
        loadNumberArrayConfInt(grnCnl, displayMode);
        displayConfig();
        break;
      }
    case MODE_BLU_CNL: {
        if (button2.isButtonPressedAndReleased()) {
          bluCnl++;
          if (bluCnl > COLOUR_CNL_MAX) {
            bluCnl = COLOUR_CNL_MIN;
          }
        }
        loadNumberArrayConfInt(bluCnl, displayMode);
        displayConfig();
        break;
      }
    case MODE_CYCLE_SPEED: {
        if (button2.isButtonPressedAndReleased()) {
          cycleSpeed = cycleSpeed + 2;
          if (cycleSpeed > CYCLE_SPEED_MAX) {
            cycleSpeed = CYCLE_SPEED_MIN;
          }
        }
        loadNumberArrayConfInt(cycleSpeed, displayMode);
        displayConfig();
        break;
      }
    case MODE_MIN_DIM_UP: {
        if (button2.isButtonPressedAndReleased()) {
          minDim += 10;
          if (minDim > MIN_DIM_MAX) {
            minDim = MIN_DIM_MAX;
          }
        }
        loadNumberArrayConfInt(minDim, displayMode);
        displayConfig();
        break;
      }
    case MODE_MIN_DIM_DOWN: {
        if (button2.isButtonPressedAndReleased()) {
          minDim -= 10;
          if (minDim < MIN_DIM_MIN) {
            minDim = MIN_DIM_MIN;
          }
        }
        loadNumberArrayConfInt(minDim, displayMode);
        displayConfig();
        break;
      }
    case MODE_USE_PIR_PULLUP: {
        if (button2.isButtonPressedAndReleased()) {
          usePIRPullup = !usePIRPullup;
        }
        loadNumberArrayConfBool(usePIRPullup, displayMode);
        digitalWrite(pirPin, usePIRPullup);
        displayConfig();
        break;
      }
    case MODE_TEMP: {
        loadNumberArrayTemp(displayMode);
        displayConfig();
        break;
      }
    case MODE_VERSION: {
        loadNumberArrayConfInt(SOFTWARE_VERSION, displayMode);
        displayConfig();
        break;
      }
    case MODE_TUBE_TEST: {
        allNormal(DO_NOT_APPLY_LEAD_0_BLANK);
        loadNumberArrayTestDigits();
        break;
      }
  }
}

// ************************************************************
// output a PWM LED channel, adjusting for dimming and PWM
// brightness:
// rawValue: The raw brightness value between 0 - 255
// ledPWMVal: The pwm factor between 0 - 1
// dimFactor: The dimming value between 0 - 1
// ************************************************************
byte getLEDAdjusted(float rawValue, float ledPWMVal, float dimFactor) {
  byte dimmedPWMVal = (byte)(rawValue * ledPWMVal * dimFactor);
  return dim_curve[dimmedPWMVal];
}

// ************************************************************
// Colour cycling 3: one colour dominates
// ************************************************************
void cycleColours3(int colors[3]) {
  cycleCount++;
  if (cycleCount > cycleSpeed) {
    cycleCount = 0;

    if (changeSteps == 0) {
      changeSteps = random(256);
      currentColour = random(3);
    }

    changeSteps--;

    switch (currentColour) {
      case 0:
        if (colors[0] < 255) {
          colors[0]++;
          if (colors[1] > 0) {
            colors[1]--;
          }
          if (colors[2] > 0) {
            colors[2]--;
          }
        } else {
          changeSteps = 0;
        }
        break;
      case 1:
        if (colors[1] < 255) {
          colors[1]++;
          if (colors[0] > 0) {
            colors[0]--;
          }
          if (colors[2] > 0) {
            colors[2]--;
          }
        } else {
          changeSteps = 0;
        }
        break;
      case 2:
        if (colors[2] < 255) {
          colors[2]++;
          if (colors[0] > 0) {
            colors[0]--;
          }
          if (colors[1] > 0) {
            colors[1]--;
          }
        } else {
          changeSteps = 0;
        }
        break;
    }
  }
}

//**********************************************************************************
//**********************************************************************************
//*                             Utility functions                                  *
//**********************************************************************************
//**********************************************************************************

// ************************************************************
// Break the time into displayable digits
// ************************************************************
void loadNumberArrayTime() {
  numberArray[S1] = second() % 10;
  numberArray[S10] = second() / 10;
  numberArray[M1] = minute() % 10;
  numberArray[M10] = minute() / 10;
  if (hourMode) {
    numberArray[H1] = hourFormat12() % 10;
    numberArray[H10] = hourFormat12() / 10;
  } else {
    numberArray[H1] = hour() % 10;
    numberArray[H10] = hour() / 10;
  }
}

// ************************************************************
// Show the value we got over I2C
// ************************************************************
void loadNumberArrayValue() {
  numberArray[S1] = valueToShow[2] & 0xf;
  numberArray[S10] = valueToShow[2] >> 4;
  numberArray[M1] = valueToShow[1] & 0xf;
  numberArray[M10] = valueToShow[1] >> 4;
  numberArray[H1] = valueToShow[0] & 0xf;
  numberArray[H10] = valueToShow[0] >> 4;
}

// ************************************************************
// Break the time into displayable digits
// ************************************************************
void loadNumberArraySameValue(byte val) {
  numberArray[S1] = val;
  numberArray[S10] = val;
  numberArray[M1] = val;
  numberArray[M10] = val;
  numberArray[H1] = val;
  numberArray[H10] = val;
}

// ************************************************************
// Break the time into displayable digits
// ************************************************************
void loadNumberArrayDate() {
  switch (dateFormat) {
    case DATE_FORMAT_YYMMDD:
      numberArray[S1] = day() % 10;
      numberArray[S10] = day() / 10;
      numberArray[M1] = month() % 10;
      numberArray[M10] = month() / 10;
      numberArray[H1] = (year() - 2000) % 10;
      numberArray[H10] = (year() - 2000) / 10;
      break;
    case DATE_FORMAT_MMDDYY:
      numberArray[S1] = (year() - 2000) % 10;
      numberArray[S10] = (year() - 2000) / 10;
      numberArray[M1] = day() % 10;
      numberArray[M10] = day() / 10;
      numberArray[H1] = month() % 10;
      numberArray[H10] = month() / 10;
      break;
    case DATE_FORMAT_DDMMYY:
      numberArray[S1] = (year() - 2000) % 10;
      numberArray[S10] = (year() - 2000) / 10;
      numberArray[M1] = month() % 10;
      numberArray[M10] = month() / 10;
      numberArray[H1] = day() % 10;
      numberArray[H10] = day() / 10;
      break;
  }
}

// ************************************************************
// Break the temperature into displayable digits
// ************************************************************
void loadNumberArrayTemp(int confNum) {
  numberArray[S1] = (confNum) % 10;
  numberArray[S10] = (confNum / 10) % 10;
  float temp = getRTCTemp();
  int wholeDegrees = int(temp);
  temp = (temp - float(wholeDegrees)) * 100.0;
  int fractDegrees = int(temp);

  numberArray[M1] = fractDegrees % 10;
  numberArray[M10] =  fractDegrees / 10;
  numberArray[H1] =  wholeDegrees % 10;
  numberArray[H10] = wholeDegrees / 10;
}

// ************************************************************
// Break the LDR reading into displayable digits
// ************************************************************
void loadNumberArrayLDR() {
  numberArray[S1] = 0;
  numberArray[S10] = 0;

  numberArray[M1] = (ldrValue / 1) % 10;
  numberArray[M10] = (ldrValue / 10) % 10;
  numberArray[H1] = (ldrValue / 100) % 10;
  numberArray[H10] = (ldrValue / 1000) % 10;
}

// ************************************************************
// Test digits
// ************************************************************
void loadNumberArrayTestDigits() {
  numberArray[S1] =  second() % 10;
  numberArray[S10] = (second() + 1) % 10;
  numberArray[M1] = (second() + 2) % 10;
  numberArray[M10] = (second() + 3) % 10;
  numberArray[H1] = (second() + 4) % 10;
  numberArray[H10] = (second() + 5) % 10;
}

// ************************************************************
// Do the Anti Cathode Poisoning
// ************************************************************
void loadNumberArrayACP() {
  numberArray[S1] = (second() + acpOffset) % 10;
  numberArray[S10] = (second() / 10 + acpOffset) % 10;
  numberArray[M1] = (minute() + acpOffset) % 10;
  numberArray[M10] = (minute() / 10 + acpOffset) % 10;
  numberArray[H1] = (hour() + acpOffset)  % 10;
  numberArray[H10] = (hour() / 10 + acpOffset) % 10;
}

// ************************************************************
// Show an integer configuration value
// ************************************************************
void loadNumberArrayConfInt(int confValue, int confNum) {
  numberArray[S1] = (confNum) % 10;
  numberArray[S10] = (confNum / 10) % 10;
  numberArray[M1] = (confValue / 1) % 10;
  numberArray[M10] = (confValue / 10) % 10;
  numberArray[H1] = (confValue / 100) % 10;
  numberArray[H10] = (confValue / 1000) % 10;
}

// ************************************************************
// Show a bool configuration value
// ************************************************************
void loadNumberArrayConfBool(bool confValue, int confNum) {
  int boolInt;
  if (confValue) {
    boolInt = 1;
  } else {
    boolInt = 0;
  }
  numberArray[S1] = (confNum) % 10;
  numberArray[S10] = (confNum / 10) % 10;
  numberArray[M1] = boolInt;
  numberArray[M10] = 0;
  numberArray[H1] = 0;
  numberArray[H10] = 0;
}

// ************************************************************
// Show an integer configuration value
// ************************************************************
void loadNumberArrayIP(byte byte1, byte byte2) {
  numberArray[S1] = (byte2) % 10;
  numberArray[S10] = (byte2 / 10) % 10;
  numberArray[M1] = (byte2 / 100) % 10;
  numberArray[M10] = (byte1) % 10;
  numberArray[H1] = (byte1 / 10) % 10;
  numberArray[H10] = (byte1 / 100) % 10;
}

// ************************************************************
// Do a single complete display, including any fading and
// dimming requested. Performs the display loop
// DIGIT_DISPLAY_COUNT times for each digit, with no delays.
// This is the heart of the display processing!
// ************************************************************
void outputDisplay() {
  byte tmpDispType;
  byte tmpDispTypeArray[DIGIT_COUNT];

  long long outputValue = 0;
  long long outputValueCurr = 0;

  for ( int i = DIGIT_COUNT - 1 ; i >= 0  ; i -- ) {
    // Blanking
    if (blankTubes) {
      tmpDispType = BLANKED;
    } else {
      tmpDispType = displayType[i];
    }

    // Digit blinking
    if (tmpDispType == BLINK) {
      if (blinkState) {
      } else {
        tmpDispType = BLANKED;
      }
    }

    // Trigger scolling and fading - scolling takes precendence
    if (numberArray[i] != currNumberArray[i]) {
      // Do scrollback when we are going to 0
      if ((numberArray[i] == 0) && scrollback && (scrollCounter[i] == 0)) {
          scrollCounter[i] = (currNumberArray[i]+1) * scrollSteps;
      } else if ((fadeState == 0) && fade) {
        // if we are not going to 0, set up the fade steps
        fadeState = PHASE_MAX * fadeStepsInternal;
      } else if (fadeState == 0) {
        currNumberArray[i] = numberArray[i];
      }
    }

    if (scrollCounter[i] > 0) {
      scrollCounter[i] = scrollCounter[i] - 1;
      currNumberArray[i] = scrollCounter[i]/scrollSteps;
    }

    tmpDispTypeArray[i] = tmpDispType;
  }

  if (fadeState == 1) {
    fadeState = 0;
    switchTimeBuf = 0;
    for (byte j = 0 ; j < DIGIT_COUNT ; j++) {
      if (scrollCounter[j] == 0) {
        currNumberArray[j] = numberArray[j];
      }
    }
  } else if (fadeState > 0) {
    fadeState--;
    switchTimeBuf = (fadeState / fadeStepsInternal);
  }

#ifdef NORMAL_DISPLAY
  for ( int i = 0 ; i < DIGIT_COUNT  ; i++ ) {
#else
  for ( int i = DIGIT_COUNT - 1 ; i >= 0 ; i-- ) {
#endif
    outputValue = outputValue << 10;
    outputValueCurr = outputValueCurr << 10;

    if (tmpDispTypeArray[DIGIT_COUNT - 1 - i] != BLANKED) {
      byte digitIdx;
      long long digit;

      if (scrollCounter[DIGIT_COUNT - 1 - i] > 0) {
        digitIdx = currNumberArray[DIGIT_COUNT - 1 - i];
      } else {
        digitIdx = numberArray[DIGIT_COUNT - 1 - i];
      }
      digit = digits[digitIdx];
      outputValue = outputValue | digit;

      digitIdx = currNumberArray[DIGIT_COUNT - 1 - i];
      digit = digits[digitIdx];
      outputValueCurr = outputValueCurr | digit;
    }
  }

  if (blinkState) {
    long long leds = 0xf000000000000000;
    outputValue = outputValue | leds;
    outputValueCurr = outputValueCurr | leds;
  }

  outputBuf = outputValue;
  outputCurrBuf = outputValueCurr;
}

// ************************************************************
// Display preset - apply leading zero blanking
// ************************************************************
void applyBlanking() {
  // If we are not blanking, just get out
  if (blankLeading == false) {
    return;
  }

  // We only want to blank the hours tens digit
  if (numberArray[H10] == 0) {
      displayType[H10] = BLANKED;
  }
}

// ************************************************************
// highlight years taking into account the date format
// ************************************************************
void highlightYearsDateFormat() {
  switch (dateFormat) {
    case DATE_FORMAT_YYMMDD:
      highlightHrs();
      break;
    case DATE_FORMAT_MMDDYY:
      highlightSecs();
      break;
    case DATE_FORMAT_DDMMYY:
      highlightSecs();
      break;
  }
}

// ************************************************************
// highlight years taking into account the date format
// ************************************************************
void highlightMonthsDateFormat() {
  switch (dateFormat) {
    case DATE_FORMAT_YYMMDD:
      highlightMins();
      break;
    case DATE_FORMAT_MMDDYY:
      highlightHrs();
      break;
    case DATE_FORMAT_DDMMYY:
      highlightMins();
      break;
  }
}

// ************************************************************
// highlight days taking into account the date format
// ************************************************************
void highlightDaysDateFormat() {
  switch (dateFormat) {
    case DATE_FORMAT_YYMMDD:
      highlightSecs();
      break;
    case DATE_FORMAT_MMDDYY:
      highlightMins();
      break;
    case DATE_FORMAT_DDMMYY:
      highlightHrs();
      break;
  }
}

// ************************************************************
// Display preset, highlight the seconds digits
// ************************************************************
void highlightSecs() {
  setAllType(NORMAL);
  displayType[S10] = BLINK;
  displayType[S1] = BLINK;
}

// ************************************************************
// Display preset, highlight digits 2 and 3
// ************************************************************
void highlightMins() {
  setAllType(NORMAL);
  displayType[M10] = BLINK;
  displayType[M1] = BLINK;
}

// ************************************************************
// Display preset, highlight digits 4 and 5
// ************************************************************
void highlightHrs() {
  setAllType(NORMAL);
  displayType[H10] = BLINK;
  displayType[H1] = BLINK;
}

// ************************************************************
// Display preset
// ************************************************************
void allNormal(bool blanking) {
  setAllType(NORMAL);

  if (blanking) {
    applyBlanking();
  }  
}

// ************************************************************
// Display preset
// ************************************************************
void displayConfig() {
  setAllType(NORMAL);
  displayType[S10] = BLINK;
  displayType[S1]  = BLINK;
}

// ************************************************************
// Display preset
// ************************************************************
void allBlanked() {
  setAllType(BLANKED);
}

// ************************************************************
// Display preset: set all digits to a given value
// ************************************************************
void setAllType(byte newType) {
  for (int i = 0 ; i < DIGIT_COUNT ; i++) displayType[i] = newType;
}

// ************************************************************
// Display preset
// ************************************************************
void loadDisplayConfigValue() {
  displayType[5] = valueDisplayType[2] & 0xf;
  displayType[4] = valueDisplayType[2] >> 4;
  displayType[5] = valueDisplayType[1] & 0xf;
  displayType[2] = valueDisplayType[1] >> 4;
  displayType[1] = valueDisplayType[0] & 0xf;
  displayType[0] = valueDisplayType[0] >> 4;
}

// ************************************************************
// Reset the seconds to 00
// ************************************************************
void resetSecond() {
  byte tmpSecs = 0;
  setTime(hour(), minute(), tmpSecs, day(), month(), year());
  setRTC();
}

// ************************************************************
// increment the time by 1 Sec
// ************************************************************
void incSecond() {
  byte tmpSecs = second();
  tmpSecs++;
  if (tmpSecs >= SECS_MAX) {
    tmpSecs = 0;
  }
  setTime(hour(), minute(), tmpSecs, day(), month(), year());
  setRTC();
}

// ************************************************************
// increment the time by 1 min
// Resets the second
// ************************************************************
void incMins() {
  byte tmpMins = minute();
  tmpMins++;
  if (tmpMins >= MINS_MAX) {
    tmpMins = 0;
  }
  setTime(hour(), tmpMins, 0, day(), month(), year());
  setRTC();
}

// ************************************************************
// decrement the time by 1 min
// Resets the second
// ************************************************************
void decMins() {
  byte tmpMins = minute();
  if (tmpMins == 0) {
    tmpMins = MINS_MAX - 1;
  } else {
    tmpMins--;
  }
  setTime(hour(), tmpMins, 0, day(), month(), year());
  setRTC();
}

// ************************************************************
// increment the time by 1 hour
// ************************************************************
void incHours() {
  byte tmpHours = hour();
  tmpHours++;

  if (tmpHours >= HOURS_MAX) {
    tmpHours = 0;
  }
  setTime(tmpHours, minute(), second(), day(), month(), year());
  setRTC();
}

// ************************************************************
// increment the time by 1 hour
// ************************************************************
void decHours() {
  byte tmpHours = hour();
  if (tmpHours == 0) {
    tmpHours = HOURS_MAX - 1;
  } else {
    tmpHours--;
  }
  setTime(tmpHours, minute(), second(), day(), month(), year());
  setRTC();
}

// ************************************************************
// increment the date by 1 day
// ************************************************************
void incDays() {
  byte tmpDays = day();
  tmpDays++;

  int maxDays;
  switch (month())
  {
    case 4:
    case 6:
    case 9:
    case 11:
      {
        maxDays = 31;
        break;
      }
    case 2:
      {
        // we won't worry about leap years!!!
        maxDays = 28;
        break;
      }
    default:
      {
        maxDays = 31;
      }
  }

  if (tmpDays > maxDays) {
    tmpDays = 1;
  }
  setTime(hour(), minute(), second(), tmpDays, month(), year());
  setRTC();
}

// ************************************************************
// increment the month by 1 month
// ************************************************************
void incMonths() {
  byte tmpMonths = month();
  tmpMonths++;

  if (tmpMonths > 12) {
    tmpMonths = 1;
  }
  setTime(hour(), minute(), second(), day(), tmpMonths, year());
  setRTC();
}

// ************************************************************
// increment the year by 1 year
// ************************************************************
void incYears() {
  byte tmpYears = year() % 100;
  tmpYears++;

  if (tmpYears > 50) {
    tmpYears = 15;
  }
  setTime(hour(), minute(), second(), day(), month(), 2000 + tmpYears);
  setRTC();
}

// ************************************************************
// Check the blanking
// ************************************************************
bool checkBlanking() {
  // Check day blanking, but only when we are in
  // normal time mode
  if (currentMode == MODE_TIME) {
    switch (dayBlanking) {
      case DAY_BLANKING_NEVER:
        return false;
      case DAY_BLANKING_HOURS:
        return getHoursBlanked();
      case DAY_BLANKING_WEEKEND:
        return ((weekday() == 1) || (weekday() == 7));
      case DAY_BLANKING_WEEKEND_OR_HOURS:
        return ((weekday() == 1) || (weekday() == 7)) || getHoursBlanked();
      case DAY_BLANKING_WEEKEND_AND_HOURS:
        return ((weekday() == 1) || (weekday() == 7)) && getHoursBlanked();
      case DAY_BLANKING_WEEKDAY:
        return ((weekday() > 1) && (weekday() < 7));
      case DAY_BLANKING_WEEKDAY_OR_HOURS:
        return ((weekday() > 1) && (weekday() < 7)) || getHoursBlanked();
      case DAY_BLANKING_WEEKDAY_AND_HOURS:
        return ((weekday() > 1) && (weekday() < 7)) && getHoursBlanked();
      case DAY_BLANKING_ALWAYS:
        return true;
    }
  }
}

// ************************************************************
// If we are currently blanked based on hours
// ************************************************************
bool getHoursBlanked() {
  if (blankHourStart > blankHourEnd) {
    // blanking before midnight
    return ((hour() >= blankHourStart) || (hour() < blankHourEnd));
  } else if (blankHourStart < blankHourEnd) {
    // dim at or after midnight
    return ((hour() >= blankHourStart) && (hour() < blankHourEnd));
  } else {
    // no dimming if Start = End
    return false;
  }
}

// ************************************************************
// Set the tubes and LEDs blanking variables based on blanking mode and 
// blank mode settings
// ************************************************************
void setTubesAndLEDSBlankMode() {
  if (blanked) {
    switch(blankMode) {
      case BLANK_MODE_TUBES:
      {
        blankTubes = true;
        blankLEDs = false;
        break;
      }
      case BLANK_MODE_LEDS:
      {
        blankTubes = false;
        blankLEDs = true;
        break;
      }
      case BLANK_MODE_BOTH:
      {
        blankTubes = true;
        blankLEDs = true;
        break;
      }
    }
  } else {
    blankTubes = false;
    blankLEDs = false;
  }
}

void randomRGBFlash(int delayVal) {
  if (random(3) == 0) {
    setAllLEDs(255,0,0);
  }
  if (random(3) == 0) {
    setAllLEDs(0,255,0);
  }
  if (random(3) == 0) {
    setAllLEDs(0,0,255);
  }
  delay(delayVal);
  setAllLEDs(0,0,0);
  delay(delayVal);
}

// ************************************************************
// Jump to a new position in the menu - used to skip unused items
// ************************************************************
void setNewNextMode(int newNextMode) {
  nextMode = newNextMode;
  currentMode = newNextMode - 1;
}

//**********************************************************************************
//**********************************************************************************
//*                         RTC Module Time Provider                               *
//**********************************************************************************
//**********************************************************************************

// ************************************************************
// Get the time from the RTC
// ************************************************************
void getRTCTime() {
  // Start the RTC communication in master mode
  Wire.end();
  Wire.begin();

  // Set up the time provider
  // first try to find the RTC, if not available, go into slave mode
  Wire.beginTransmission(RTC_I2C_ADDRESS);
  useRTC = (Wire.endTransmission() == 0);
  if (useRTC) {
    bool PM;
    bool twentyFourHourClock;
    bool century = false;

    byte years = Clock.getYear() + 2000;
    byte months = Clock.getMonth(century);
    byte days = Clock.getDate();
    byte hours = Clock.getHour(twentyFourHourClock, PM);
    byte mins = Clock.getMinute();
    byte secs = Clock.getSecond();
    setTime(hours, mins, secs, days, months, years);

    // Make sure the clock keeps running even on battery
    if (!Clock.oscillatorCheck())
      Clock.enableOscillator(true, true, 0);
  }

  // Return back to I2C in slave mode
  Wire.end();
  Wire.begin(I2C_SLAVE_ADDR);
  Wire.onReceive(receiveEvent);
  Wire.onRequest(requestEvent);
}

// ************************************************************
// Set the date/time in the RTC from the internal time
// Always hold the time in 24 format, we convert to 12 in the
// display.
//
// Because we are usually in I2C slave mode (for the Wifi module)
// we temporarily have to switch back into master mode to be able
// to control the RTC.
// ************************************************************
void setRTC() {
  if (useRTC) {
    // Start the RTC communication in master mode
    Wire.end();
    Wire.begin();

    // Set up the time provider
    // first try to find the RTC, if not available, go into slave mode
    Wire.beginTransmission(RTC_I2C_ADDRESS);
    Clock.setClockMode(false); // false = 24h
    Clock.setYear(year() % 100);
    Clock.setMonth(month());
    Clock.setDate(day());
    Clock.setDoW(weekday());
    Clock.setHour(hour());
    Clock.setMinute(minute());
    Clock.setSecond(second());

    Wire.endTransmission();
    Wire.end();

    // Go back into slave mode
    Wire.begin(I2C_SLAVE_ADDR);
    Wire.onReceive(receiveEvent);
    Wire.onRequest(requestEvent);
  }
}

// ************************************************************
// Get the temperature from the RTC
// ************************************************************
float getRTCTemp() {
  if (useRTC) {
    return Clock.getTemperature();
  } else {
    return 0.0;
  }
}

//**********************************************************************************
//**********************************************************************************
//*                               EEPROM interface                                 *
//**********************************************************************************
//**********************************************************************************

// ************************************************************
// Save current values back to EEPROM
// ************************************************************
void saveEEPROMValues() {
  EEPROM.write(EE_12_24, hourMode);
  EEPROM.write(EE_FADE_STEPS, fadeStepsExternal);
  EEPROM.write(EE_DATE_FORMAT, dateFormat);
  EEPROM.write(EE_DAY_BLANKING, dayBlanking);
  EEPROM.write(EE_DIM_DARK_LO, dimDark % 256);
  EEPROM.write(EE_DIM_DARK_HI, dimDark / 256);
  EEPROM.write(EE_BLANK_LEAD_ZERO, blankLeading);
  EEPROM.write(EE_SCROLLBACK, scrollback);
  EEPROM.write(EE_FADE, fade);
  EEPROM.write(EE_SCROLL_STEPS, scrollSteps);
  EEPROM.write(EE_DIM_BRIGHT_LO, dimBright % 256);
  EEPROM.write(EE_DIM_BRIGHT_HI, dimBright / 256);
  EEPROM.write(EE_DIM_SMOOTH_SPEED, sensorSmoothCountLDR);
  EEPROM.write(EE_RED_INTENSITY, redCnl);
  EEPROM.write(EE_GRN_INTENSITY, grnCnl);
  EEPROM.write(EE_BLU_INTENSITY, bluCnl);
  EEPROM.write(EE_BACKLIGHT_MODE, backlightMode);
  EEPROM.write(EE_SUPPRESS_ACP, suppressACP);
  EEPROM.write(EE_HOUR_BLANK_START, blankHourStart);
  EEPROM.write(EE_HOUR_BLANK_END, blankHourEnd);
  EEPROM.write(EE_CYCLE_SPEED, cycleSpeed);
  EEPROM.write(EE_MIN_DIM_LO, minDim % 256);
  EEPROM.write(EE_MIN_DIM_HI, minDim / 256);
  EEPROM.write(EE_PIR_TIMEOUT_LO, pirTimeout % 256);
  EEPROM.write(EE_PIR_TIMEOUT_HI, pirTimeout / 256);
  EEPROM.write(EE_USE_LDR, useLDR);
  EEPROM.write(EE_BLANK_MODE, blankMode);
  EEPROM.write(EE_SLOTS_MODE, slotsMode);
  EEPROM.write(EE_PIR_PULLUP, usePIRPullup);
}

// ************************************************************
// read EEPROM values
// ************************************************************
void readEEPROMValues() {
  hourMode = EEPROM.read(EE_12_24);

  fadeStepsExternal = EEPROM.read(EE_FADE_STEPS);
  if ((fadeStepsExternal < FADE_STEPS_MIN) || (fadeStepsExternal > FADE_STEPS_MAX)) {
    fadeStepsExternal = FADE_STEPS_DEFAULT;
  }
  fadeStepsInternal = fadeStepsExternal / 10;

  dateFormat = EEPROM.read(EE_DATE_FORMAT);
  if ((dateFormat < DATE_FORMAT_MIN) || (dateFormat > DATE_FORMAT_MAX)) {
    dateFormat = DATE_FORMAT_DEFAULT;
  }

  dayBlanking = EEPROM.read(EE_DAY_BLANKING);
  if ((dayBlanking < DAY_BLANKING_MIN) || (dayBlanking > DAY_BLANKING_MAX)) {
    dayBlanking = DAY_BLANKING_DEFAULT;
  }

  dimDark = EEPROM.read(EE_DIM_DARK_HI) * 256 + EEPROM.read(EE_DIM_DARK_LO);
  if ((dimDark < SENSOR_LOW_MIN) || (dimDark > SENSOR_LOW_MAX)) {
    dimDark = SENSOR_LOW_DEFAULT;
  }

  blankLeading = EEPROM.read(EE_BLANK_LEAD_ZERO);
  scrollback = EEPROM.read(EE_SCROLLBACK);
  fade = EEPROM.read(EE_FADE);

  scrollSteps = EEPROM.read(EE_SCROLL_STEPS);
  if ((scrollSteps < SCROLL_STEPS_MIN) || (scrollSteps > SCROLL_STEPS_MAX)) {
    scrollSteps = SCROLL_STEPS_DEFAULT;
  }

  dimBright = EEPROM.read(EE_DIM_BRIGHT_HI) * 256 + EEPROM.read(EE_DIM_BRIGHT_LO);
  if ((dimBright < SENSOR_HIGH_MIN) || (dimBright > SENSOR_HIGH_MAX)) {
    dimBright = SENSOR_HIGH_DEFAULT;
  }

  sensorSmoothCountLDR = EEPROM.read(EE_DIM_SMOOTH_SPEED);
  if ((sensorSmoothCountLDR < SENSOR_SMOOTH_READINGS_MIN) || (sensorSmoothCountLDR > SENSOR_SMOOTH_READINGS_MAX)) {
    sensorSmoothCountLDR = SENSOR_SMOOTH_READINGS_DEFAULT;
  }

  backlightMode = EEPROM.read(EE_BACKLIGHT_MODE);
  if ((backlightMode < BACKLIGHT_MIN) || (backlightMode > BACKLIGHT_MAX)) {
    backlightMode = BACKLIGHT_DEFAULT;
  }

  redCnl = EEPROM.read(EE_RED_INTENSITY);
  if ((redCnl < COLOUR_CNL_MIN) || (redCnl > COLOUR_CNL_MAX)) {
    redCnl = COLOUR_RED_CNL_DEFAULT;
  }

  grnCnl = EEPROM.read(EE_GRN_INTENSITY);
  if ((grnCnl < COLOUR_CNL_MIN) || (grnCnl > COLOUR_CNL_MAX)) {
    grnCnl = COLOUR_GRN_CNL_DEFAULT;
  }

  bluCnl = EEPROM.read(EE_BLU_INTENSITY);
  if ((bluCnl < COLOUR_CNL_MIN) || (bluCnl > COLOUR_CNL_MAX)) {
    bluCnl = COLOUR_BLU_CNL_DEFAULT;
  }

  suppressACP = EEPROM.read(EE_SUPPRESS_ACP);

  blankHourStart = EEPROM.read(EE_HOUR_BLANK_START);
  if ((blankHourStart < 0) || (blankHourStart > HOURS_MAX)) {
    blankHourStart = 0;
  }

  blankHourEnd = EEPROM.read(EE_HOUR_BLANK_END);
  if ((blankHourEnd < 0) || (blankHourEnd > HOURS_MAX)) {
    blankHourEnd = 7;
  }

  cycleSpeed = EEPROM.read(EE_CYCLE_SPEED);
  if ((cycleSpeed < CYCLE_SPEED_MIN) || (cycleSpeed > CYCLE_SPEED_MAX)) {
    cycleSpeed = CYCLE_SPEED_DEFAULT;
  }

  minDim = EEPROM.read(EE_MIN_DIM_HI) * 256 + EEPROM.read(EE_MIN_DIM_LO);
  if ((minDim < MIN_DIM_MIN) || (minDim > MIN_DIM_MAX)) {
    minDim = MIN_DIM_DEFAULT;
  }

  pirTimeout = EEPROM.read(EE_PIR_TIMEOUT_HI) * 256 + EEPROM.read(EE_PIR_TIMEOUT_LO);
  if ((pirTimeout < PIR_TIMEOUT_MIN) || (pirTimeout > PIR_TIMEOUT_MAX)) {
    pirTimeout = PIR_TIMEOUT_DEFAULT;
  }

  minDim = EEPROM.read(EE_MIN_DIM_HI) * 256 + EEPROM.read(EE_MIN_DIM_LO);
  if ((minDim < MIN_DIM_MIN) || (minDim > MIN_DIM_MAX)) {
    minDim = MIN_DIM_DEFAULT;
  }

  blankMode= EEPROM.read(EE_BLANK_MODE);
  if ((blankMode < BLANK_MODE_MIN) || (blankMode > BLANK_MODE_MAX)) {
    blankMode = BLANK_MODE_DEFAULT;
  }

  useLDR = EEPROM.read(EE_USE_LDR);

  slotsMode= EEPROM.read(EE_SLOTS_MODE);
  if ((slotsMode < SLOTS_MODE_MIN) || (slotsMode > SLOTS_MODE_MAX)) {
    slotsMode = SLOTS_MODE_DEFAULT;
  }

  usePIRPullup = EEPROM.read(EE_PIR_PULLUP);
}

// ************************************************************
// Reset EEPROM values back to what they once were
// ************************************************************
void factoryReset() {
  hourMode = HOUR_MODE_DEFAULT;
  blankLeading = LEAD_BLANK_DEFAULT;
  scrollback = SCROLLBACK_DEFAULT;
  fade = FADE_DEFAULT;
  fadeStepsExternal = FADE_STEPS_DEFAULT;
  fadeStepsInternal = fadeStepsExternal / 10;
  dateFormat = DATE_FORMAT_DEFAULT;
  dayBlanking = DAY_BLANKING_DEFAULT;
  dimDark = SENSOR_LOW_DEFAULT;
  scrollSteps = SCROLL_STEPS_DEFAULT;
  dimBright = SENSOR_HIGH_DEFAULT;
  sensorSmoothCountLDR = SENSOR_SMOOTH_READINGS_DEFAULT;
  dateFormat = DATE_FORMAT_DEFAULT;
  dayBlanking = DAY_BLANKING_DEFAULT;
  backlightMode = BACKLIGHT_DEFAULT;
  redCnl = COLOUR_RED_CNL_DEFAULT;
  grnCnl = COLOUR_GRN_CNL_DEFAULT;
  bluCnl = COLOUR_BLU_CNL_DEFAULT;
  suppressACP = SUPPRESS_ACP_DEFAULT;
  blankHourStart = 0;
  blankHourEnd = 7;
  cycleSpeed = CYCLE_SPEED_DEFAULT;
  minDim = MIN_DIM_DEFAULT;
  pirTimeout = PIR_TIMEOUT_DEFAULT;
  useLDR = USE_LDR_DEFAULT;
  blankMode = BLANK_MODE_DEFAULT;
  slotsMode = SLOTS_MODE_DEFAULT;
  usePIRPullup = USE_PIR_PULLUP_DEFAULT;

  saveEEPROMValues();
}

//**********************************************************************************
//**********************************************************************************
//*                          Light Dependent Resistor                              *
//**********************************************************************************
//**********************************************************************************

// ******************************************************************
// Check the ambient light through the LDR (Light Dependent Resistor)
// Smooths the reading over several reads.
//
// The LDR in bright light gives reading of around 50, the reading in
// total darkness is around 900.
//
// The return value is the dimming count we are using. 999 is full
// brightness, 100 is very dim.
//
// Because the floating point calculation may return more than the
// maximum value, we have to clamp it as the final step
// ******************************************************************
int getDimmingFromLDR() {
  if (useLDR) {
    int rawSensorVal = 1023 - analogRead(LDRPin);
    double sensorDiff = rawSensorVal - sensorLDRSmoothed;
    sensorLDRSmoothed += (sensorDiff / sensorSmoothCountLDR);

    // Scaling offset increases the base brightness
    // factor increases the sensitivity
    // double offset = current_config.thresholdBright;
    // double factor = current_config.sensitivityLDR / 5.0;
    double offset = 0.0;
    double factor = 1.0;

    int returnValue = (sensorLDRSmoothed + offset) / factor;
    
    if (returnValue < minDim) returnValue = minDim;
    if (returnValue > MAX_DIM) returnValue = MAX_DIM;
    return returnValue;
  } else {
    return 999;
  }
}

//**********************************************************************************
//**********************************************************************************
//*                                 I2C interface                                  *
//**********************************************************************************
//**********************************************************************************

/**
 * receive information from the master
 */
void receiveEvent(int bytes) {
  // the operation tells us what we are getting
  int operation = Wire.read();

  if (operation == I2C_TIME_UPDATE) {
    // If we're getting time from the WiFi module, mark that we have an active WiFi with a 5 min time out
    useWiFi = MAX_WIFI_TIME;

    int newYears = Wire.read();
    int newMonths = Wire.read();
    int newDays = Wire.read();

    int newHours = Wire.read();
    int newMins = Wire.read();
    int newSecs = Wire.read();

    setTime(newHours, newMins, newSecs, newDays, newMonths, newYears);
  } else if (operation == I2C_SET_OPTION_12_24) {
    byte readByte1224 = Wire.read();
    hourMode = (readByte1224 == 1);
    EEPROM.write(EE_12_24, hourMode);
  } else if (operation == I2C_SET_OPTION_BLANK_LEAD) {
    byte readByteBlank = Wire.read();
    blankLeading = (readByteBlank == 1);
    EEPROM.write(EE_BLANK_LEAD_ZERO, blankLeading);
  } else if (operation == I2C_SET_OPTION_SCROLLBACK) {
    byte readByteSB = Wire.read();
    scrollback = (readByteSB == 1);
    EEPROM.write(EE_SCROLLBACK, scrollback);
  } else if (operation == I2C_SET_OPTION_SUPPRESS_ACP) {
    byte readByteSA = Wire.read();
    suppressACP = (readByteSA == 1);
    EEPROM.write(EE_SUPPRESS_ACP, suppressACP);
  } else if (operation == I2C_SET_OPTION_DATE_FORMAT) {
    dateFormat = Wire.read();
    EEPROM.write(EE_DATE_FORMAT, dateFormat);
  } else if (operation == I2C_SET_OPTION_DAY_BLANKING) {
    dayBlanking = Wire.read();
    EEPROM.write(EE_DAY_BLANKING, dayBlanking);
  } else if (operation == I2C_SET_OPTION_BLANK_START) {
    blankHourStart = Wire.read();
    EEPROM.write(EE_HOUR_BLANK_START, blankHourStart);
  } else if (operation == I2C_SET_OPTION_BLANK_END) {
    blankHourEnd = Wire.read();
    EEPROM.write(EE_HOUR_BLANK_END, blankHourEnd);
  } else if (operation == I2C_SET_OPTION_FADE_STEPS) {
    fadeStepsExternal = Wire.read();
    fadeStepsInternal = fadeStepsExternal/10;
    EEPROM.write(EE_FADE_STEPS, fadeStepsExternal);
  } else if (operation == I2C_SET_OPTION_SCROLL_STEPS) {
    scrollSteps = Wire.read();
    EEPROM.write(EE_SCROLL_STEPS, scrollSteps);
  } else if (operation == I2C_SET_OPTION_BACKLIGHT_MODE) {
    backlightMode = Wire.read();
    EEPROM.write(EE_BACKLIGHT_MODE, backlightMode);
  } else if (operation == I2C_SET_OPTION_RED_CHANNEL) {
    redCnl = Wire.read();
    EEPROM.write(EE_RED_INTENSITY, redCnl);
  } else if (operation == I2C_SET_OPTION_GREEN_CHANNEL) {
    grnCnl = Wire.read();
    EEPROM.write(EE_GRN_INTENSITY, grnCnl);
  } else if (operation == I2C_SET_OPTION_BLUE_CHANNEL) {
    bluCnl = Wire.read();
    EEPROM.write(EE_BLU_INTENSITY, bluCnl);
  } else if (operation == I2C_SET_OPTION_CYCLE_SPEED) {
    cycleSpeed = Wire.read();
    EEPROM.write(EE_CYCLE_SPEED, cycleSpeed);
  } else if (operation == I2C_SHOW_IP_ADDR) {
    ourIP[0] = Wire.read();
    ourIP[1] = Wire.read();
    ourIP[2] = Wire.read();
    ourIP[3] = Wire.read();
  } else if (operation == I2C_SET_OPTION_PIR_TIMEOUT) {
    byte pirTimeoutHi = Wire.read();
    byte pirTimeoutLo = Wire.read();
    pirTimeout = pirTimeoutHi*256 + pirTimeoutLo;
    EEPROM.write(EE_PIR_TIMEOUT_HI, pirTimeoutHi);
    EEPROM.write(EE_PIR_TIMEOUT_LO, pirTimeoutLo);
  } else if (operation == I2C_SET_OPTION_MIN_DIM) {
    byte minDimHi = Wire.read();
    byte minDimLo = Wire.read();
    minDim = minDimHi*256 + minDimLo;
    EEPROM.write(EE_MIN_DIM_HI, minDimHi);
    EEPROM.write(EE_MIN_DIM_LO, minDimLo);
  } else if (operation == I2C_SET_OPTION_FADE) {
    fade = Wire.read();
    EEPROM.write(EE_FADE, fade);
  } else if (operation == I2C_SET_OPTION_USE_LDR) {
    byte readByteUseLDR = Wire.read();
    useLDR = (readByteUseLDR == 1);
    EEPROM.write(EE_USE_LDR, useLDR);
  } else if (operation == I2C_SET_OPTION_BLANK_MODE) {
    blankMode = Wire.read();
    EEPROM.write(EE_BLANK_MODE, blankMode);
  } else if (operation == I2C_SET_OPTION_SLOTS_MODE) {
    slotsMode = Wire.read();
    EEPROM.write(EE_SLOTS_MODE, slotsMode);
  } else if (operation == I2C_SHOW_VALUE) {
    // Three BCD values, plus time to show in seconds
    valueToShow[0] = Wire.read();
    valueToShow[1] = Wire.read();
    valueToShow[2] = Wire.read();
    valueDisplayTime = Wire.read();
  } else if (operation == I2C_SHOW_VALUE_FORMAT) {
    // Three BCD values, using the normal display type encoding in each nibble
    valueDisplayType[0] = Wire.read();
    valueDisplayType[1] = Wire.read();
    valueDisplayType[2] = Wire.read();
    loadDisplayConfigValue();
  }
}

/**
   send information to the master
*/
void requestEvent() {
  byte configArray[I2C_DATA_SIZE];
  int idx = 0;
  configArray[idx++] = I2C_PROTOCOL_NUMBER;  // protocol version
  configArray[idx++] = encodeBooleanForI2C(hourMode);
  configArray[idx++] = encodeBooleanForI2C(blankLeading);
  configArray[idx++] = encodeBooleanForI2C(scrollback);
  configArray[idx++] = encodeBooleanForI2C(suppressACP);
  configArray[idx++] = encodeBooleanForI2C(fade);
  configArray[idx++] = dateFormat;
  configArray[idx++] = dayBlanking;
  configArray[idx++] = blankHourStart;
  configArray[idx++] = blankHourEnd;
  configArray[idx++] = fadeStepsExternal;
  configArray[idx++] = scrollSteps;
  configArray[idx++] = backlightMode;
  configArray[idx++] = redCnl;
  configArray[idx++] = grnCnl;
  configArray[idx++] = bluCnl;
  configArray[idx++] = cycleSpeed;
  configArray[idx++] = encodeBooleanForI2C(useLDR);
  configArray[idx++] = blankMode;
  configArray[idx++] = slotsMode;
  configArray[idx++] = pirTimeout / 256;
  configArray[idx++] = pirTimeout % 256;
  configArray[idx++] = minDim / 256;
  configArray[idx++] = minDim % 256;

  Wire.write(configArray, I2C_DATA_SIZE);
}

byte encodeBooleanForI2C(bool valueToProcess) {
  if (valueToProcess) {
    byte byteToSend = 1;
    return byteToSend;
  } else {
    byte byteToSend = 0;
    return byteToSend;
  }
}
