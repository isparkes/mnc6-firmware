#include "Arduino.h"
#include "ClockButton.h"

ClockButton::ClockButton(int inputPin)
{
  pinMode(inputPin, INPUT_PULLUP);
  _inputPin = inputPin;
}

// ************************************************************
// MAIN BUTTON CHECK ENTRY POINT - should be called periodically
// See if the button was pressed and debounce. We perform a
// sort of preview here, then confirm by releasing. We track
// 3 lengths of button press: momentarily, 1S and 2S.
// ************************************************************
void ClockButton::checkButton(unsigned long nowMillis)
{
  checkButtonInternal(nowMillis);
}

// ************************************************************
// Reset everything
// ************************************************************
void ClockButton::reset()
{
  resetInternal();
}

// ************************************************************
// Check if button is pressed right now (just debounce)
// ************************************************************
boolean ClockButton::isButtonPressedNow() {
  return _buttonPressedCount == _debounceCounter;
}

// ************************************************************
// Check if button is pressed momentarily
// ************************************************************
boolean ClockButton::isButtonPressed() {
  return _buttonPress;
}

// ************************************************************
// Check if button is pressed for a long time (> 1S)
// ************************************************************
boolean ClockButton::isButtonPressed1S() {
  return _buttonPress1S;
}

// ************************************************************
// Check if button is pressed for a moderately long time (> 2S)
// ************************************************************
boolean ClockButton::isButtonPressed2S() {
  return _buttonPress2S;
}

// ************************************************************
// Check if button is pressed for a very long time (> 8S)
// ************************************************************
boolean ClockButton::isButtonPressed8S() {
  return _buttonPress8S;
}

// ************************************************************
// Check if button is pressed for a short time (> 200mS) and released
// ************************************************************
boolean ClockButton::isButtonPressedAndReleased() {
  if (_buttonPressRelease) {
    _buttonPressRelease = false;
    return true;
  } else {
    return false;
  }
}

// ************************************************************
// Check if button is pressed for a long time (> 2) and released
// ************************************************************
boolean ClockButton::isButtonPressedReleased1S() {
  if (_buttonPressRelease1S) {
    _buttonPressRelease1S = false;
    return true;
  } else {
    return false;
  }
}

// ************************************************************
// Check if button is pressed for a very moderately time (> 2) and released
// ************************************************************
boolean ClockButton::isButtonPressedReleased2S() {
  if (_buttonPressRelease2S) {
    _buttonPressRelease2S = false;
    return true;
  } else {
    return false;
  }
}

// ************************************************************
// Check if button is pressed for a very long time (> 8) and released
// ************************************************************
boolean ClockButton::isButtonPressedReleased8S() {
  if (_buttonPressRelease8S) {
    _buttonPressRelease8S = false;
    return true;
  } else {
    return false;
  }
}

// ************************************************************
// ignore any presses for some milliseconds - useful for 
// menu entry and exit situations
// ************************************************************
void ClockButton::inhibit(unsigned long nowMillis, int millisToInhibit) {
  _inhibitEndTime = nowMillis + millisToInhibit;
  resetInternal();
}

// ************************************************************
// Internal processing
// ************************************************************
void ClockButton::checkButtonInternal(unsigned long nowMillis) {
  if (nowMillis < _inhibitEndTime) {
    return;
  }
  
  if (digitalRead(_inputPin) == 0) {
    _buttonWasReleased = false;

    // We need consecutive pressed counts to treat this is pressed
    if (_buttonPressedCount < _debounceCounter) {
      _buttonPressedCount += 1;
      // If we reach the debounce point, mark the start time
      if (_buttonPressedCount == _debounceCounter) {
        _buttonPressStartMillis = nowMillis;
      }
    } else {
      // We are pressed and held, maintain the press states
      if ((nowMillis - _buttonPressStartMillis) > 8000) {
        _buttonPress8S = true;
        _buttonPress2S = true;
        _buttonPress1S = true;
        _buttonPress = true;
      } else if ((nowMillis - _buttonPressStartMillis) > 2000) {
        _buttonPress8S = false;
        _buttonPress2S = true;
        _buttonPress1S = true;
        _buttonPress = true;
      } else if ((nowMillis - _buttonPressStartMillis) > 1000) {
        _buttonPress8S = false;
        _buttonPress2S = false;
        _buttonPress1S = true;
        _buttonPress = true;
      } else {
        _buttonPress8S = false;
        _buttonPress2S = false;
        _buttonPress1S = false;
        _buttonPress = true;
      }
    }
  } else {
    // mark this as a press and release if we were pressed for less than a long press
    if (_buttonPressedCount == _debounceCounter) {
      _buttonWasReleased = true;

      _buttonPressRelease8S = false;
      _buttonPressRelease2S = false;
      _buttonPressRelease1S = false;
      _buttonPressRelease = false;

      if (_buttonPress8S) {
        _buttonPressRelease8S = true;
      } else if (_buttonPress2S) {
        _buttonPressRelease2S = true;
      } else if (_buttonPress1S) {
        _buttonPressRelease1S = true;
      } else if (_buttonPress) {
        _buttonPressRelease = true;
      }
    }

    // Reset the switch flags debounce counter
    _buttonPress8S = false;
    _buttonPress2S = false;
    _buttonPress1S = false;
    _buttonPress = false;
    _buttonPressedCount = 0;
  }
}

void ClockButton::resetInternal() {
  _buttonPressRelease8S = false;
  _buttonPressRelease2S = false;
  _buttonPressRelease1S = false;
  _buttonPressRelease = false;
  _buttonPress8S = false;
  _buttonPress2S = false;
  _buttonPress1S = false;
  _buttonPress = false;
  _buttonPressedCount = 0;
}
